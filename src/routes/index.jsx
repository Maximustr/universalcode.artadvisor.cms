import Dashboard from 'layouts/Dashboard/Dashboard.jsx';
import Login from '../views/Login/Login';
import SignUp from '../views/SignUp/SignUp';
import ForgotPassword from '../views/ForgotPassword/ForgotPassword';

export const indexRoutes = [
    { path: '/', name: 'Home', component: Dashboard }
];

export const unauthorizeRoutes = [
    { path: '/login', name: 'Login', component: Login },
    { path: '/signup', name: 'SignUp', component: SignUp },
    { path: '/forgot-password', name: 'ForgotPassword', component: ForgotPassword }
]



