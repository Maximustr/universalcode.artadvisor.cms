import React, { Component } from "react";
import { Button } from "react-bootstrap";
import cx from "classnames";
import PropTypes from "prop-types";

class CustomButton extends Component {
    render() {
        const { fill, simple, pullRight, round, block, ...rest } = this.props;

        const btnClasses = cx({
            "btn-fill": fill,
            "btn-simple": simple,
            "pull-right": pullRight,
            "btn-block": block,
            "btn-round": round
        });
        let color
        switch (rest.bsStyle) {
            case 'success':
                color = '#4DB3D8'
                break
            case 'danger':
                color = '#EF8E84'
                break
            case 'secondary':
                color = '#bfbfbf'
                break
            default:
                color = null
        }
        rest.style = {
            margin: '1rem',
            fontSize: '1.8rem',
            backgroundColor: color,
            border: 'none',
            ...rest.style,
        }

        return <Button className={btnClasses} {...rest} />;
    }
}

CustomButton.propTypes = {
    fill: PropTypes.bool,
    simple: PropTypes.bool,
    pullRight: PropTypes.bool,
    block: PropTypes.bool,
    round: PropTypes.bool
};

export default CustomButton;
