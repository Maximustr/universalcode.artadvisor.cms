import React, { Component } from 'react'
import { AvForm, AvField } from 'availity-reactstrap-validation';
import Button from "components/CustomButton/CustomButton.jsx";
import TimeField from 'react-simple-timefield';

export default class MuseumSchedules extends Component {
    constructor(props) {
        super(props)
    }
    state = {
        mondayH: this.props.time.mondayH,
        tuesdayH: this.props.time.tuesdayH,
        wednesayH: this.props.time.wednesayH,
        thursdayH: this.props.time.thursdayH,
        fridayH: this.props.time.fridayH,
        saturdayH: this.props.time.saturdayH,
        sundayH: this.props.time.sundayH,

        mondayHO: this.props.time.mondayHO,
        tuesdayHO: this.props.time.tuesdayHO,
        wednesayHO: this.props.time.wednesayHO,
        thursdayHO: this.props.time.thursdayHO,
        fridayHO: this.props.time.fridayHO,
        saturdayHO: this.props.time.saturdayHO,
        sundayHO: this.props.time.sundayHO,
        showErrorMessage: false
    }
    goNecxt = () => {

        this.props.goNext({
            mondayH: this.state.mondayH,
            tuesdayH: this.state.tuesdayH,
            wednesayH: this.state.wednesayH,
            thursdayH: this.state.thursdayH,
            fridayH: this.state.fridayH,
            saturdayH: this.state.saturdayH,
            sundayH: this.state.sundayH,

            mondayHO: this.state.mondayHO,
            tuesdayHO: this.state.tuesdayHO,
            wednesayHO: this.state.wednesayHO,
            thursdayHO: this.state.thursdayHO,
            fridayHO: this.state.fridayHO,
            saturdayHO: this.state.saturdayHO,
            sundayHO: this.state.sundayHO,
        })

    }
    getFormatedTime = (time) => {
        return new Date(`January 31 1980 ${time}`)
    }
    validateIfCanGoNext = () => {
        const { mondayH, mondayHO, tuesdayH, thursdayHO, thursdayH, tuesdayHO, wednesayH, wednesayHO, fridayH, fridayHO, saturdayH, saturdayHO, sundayH, sundayHO } = this.state
        return !(
            (this.getFormatedTime(mondayH) <= this.getFormatedTime(mondayHO) &&
                this.getFormatedTime(tuesdayH) <= this.getFormatedTime(tuesdayHO) &&
                this.getFormatedTime(wednesayH) <= this.getFormatedTime(wednesayHO) &&
                this.getFormatedTime(fridayH) <= this.getFormatedTime(fridayHO) &&
                this.getFormatedTime(thursdayH) <= this.getFormatedTime(thursdayHO) &&
                this.getFormatedTime(saturdayH) <= this.getFormatedTime(saturdayHO) &&
                this.getFormatedTime(sundayH) <= this.getFormatedTime(sundayHO)) || (
                mondayH === '00:00' && mondayHO === '00:00' && tuesdayH === '00:00' && thursdayHO === '00:00' && thursdayH === '00:00' && tuesdayHO === '00:00' && wednesayH === '00:00' && wednesayHO === '00:00' && fridayH === '00:00' && fridayHO === '00:00' && saturdayH === '00:00' && saturdayHO === '00:00' && sundayH === '00:00' && sundayHO === '00:00')

        )

    }
    render() {
        const { goNext, values, goBack } = this.props
        return (
            <AvForm
                className="login-wrapper__container__form museum-details__form"
                onSubmit={(e, vals) => {
                    if (this.validateIfCanGoNext()) {
                        this.goNecxt()
                    }
                    this.setState({ showErrorMessage: true })

                }}
            >
                <div className="schedules">
                    <div className="form-input__control-group time__wrapper">
                        <span>Lunes</span>
                        <div className="form-input__control__item times">
                            <div className="single-time">
                                <label htmlFor="idc">
                                    Entrada
                            </label>
                                <TimeField
                                    value={this.state.mondayH}
                                    style={{
                                        width: '80%',
                                        padding: '5px 8px',
                                        color: '#555',
                                        border: 'solid 2px #FFF',
                                        borderRadius: '3px'
                                    }}
                                    onChange={vals => {
                                        this.setState({ mondayH: vals })
                                    }}
                                />
                            </div>
                            <div className="single-time">
                                <label htmlFor="idc">
                                    Salida
                            </label>
                                <TimeField
                                    value={this.state.mondayHO}
                                    style={{
                                        width: '80%',
                                        padding: '5px 8px',
                                        color: '#555',
                                        border: 'solid 2px #FFF',
                                        borderRadius: '3px'
                                    }}
                                    onChange={vals => {
                                        this.setState({ mondayHO: vals })
                                    }}
                                />
                            </div>

                        </div>
                    </div>
                    <div className="form-input__control-group time__wrapper">
                        <span>Martes</span>
                        <div className="form-input__control__item times">
                            <div className="single-time">
                                <label htmlFor="idc">
                                    Entrada
                            </label>
                                <TimeField
                                    value={this.state.thursdayH}
                                    style={{
                                        width: '80%',
                                        padding: '5px 8px',
                                        color: '#555',
                                        border: 'solid 2px #FFF',
                                        borderRadius: '3px'
                                    }}
                                    onChange={vals => {
                                        this.setState({ thursdayH: vals })
                                    }}
                                />
                            </div>
                            <div className="single-time">
                                <label htmlFor="idc">
                                    Salida
                            </label>
                                <TimeField
                                    value={this.state.tuesdayHO}
                                    style={{
                                        width: '80%',
                                        padding: '5px 8px',
                                        color: '#555',
                                        border: 'solid 2px #FFF',
                                        borderRadius: '3px'
                                    }}
                                    onChange={vals => {
                                        this.setState({ tuesdayHO: vals })
                                    }}
                                />
                            </div>

                        </div>
                    </div>
                    <div className="form-input__control-group time__wrapper">
                        <span>Miércoles</span>
                        <div className="form-input__control__item times">
                            <div className="single-time">
                                <label htmlFor="idc">
                                    Entrada
                            </label>
                                <TimeField
                                    value={this.state.wednesayH}
                                    style={{
                                        width: '80%',
                                        padding: '5px 8px',
                                        color: '#555',
                                        border: 'solid 2px #FFF',
                                        borderRadius: '3px'
                                    }}
                                    onChange={vals => {
                                        this.setState({ wednesayH: vals })
                                    }}
                                />
                            </div>
                            <div className="single-time">
                                <label htmlFor="idc">
                                    Salida
                            </label>
                                <TimeField
                                    value={this.state.wednesayHO}
                                    style={{
                                        width: '80%',
                                        padding: '5px 8px',
                                        color: '#555',
                                        border: 'solid 2px #FFF',
                                        borderRadius: '3px'
                                    }}
                                    onChange={vals => {
                                        this.setState({ wednesayHO: vals })
                                    }}
                                />
                            </div>

                        </div>
                    </div>
                    <div className="form-input__control-group time__wrapper">
                        <span>Jueves</span>
                        <div className="form-input__control__item times">
                            <div className="single-time">
                                <label htmlFor="idc">
                                    Entrada
                            </label>
                                <TimeField
                                    value={this.state.thursdayH}
                                    style={{
                                        width: '80%',
                                        padding: '5px 8px',
                                        color: '#555',
                                        border: 'solid 2px #FFF',
                                        borderRadius: '3px'
                                    }}
                                    onChange={vals => {
                                        this.setState({ thursdayH: vals })
                                    }}
                                />
                            </div>
                            <div className="single-time">
                                <label htmlFor="idc">
                                    Salida
                            </label>
                                <TimeField
                                    value={this.state.thursdayHO}
                                    style={{
                                        width: '80%',
                                        padding: '5px 8px',
                                        color: '#555',
                                        border: 'solid 2px #FFF',
                                        borderRadius: '3px'
                                    }}
                                    onChange={vals => {
                                        this.setState({ thursdayHO: vals })
                                    }}
                                />
                            </div>

                        </div>
                    </div>
                    <div className="form-input__control-group time__wrapper">
                        <span>Viernes</span>
                        <div className="form-input__control__item times">
                            <div className="single-time">
                                <label htmlFor="idc">
                                    Entrada
                            </label>
                                <TimeField
                                    value={this.state.fridayH}
                                    style={{
                                        width: '80%',
                                        padding: '5px 8px',
                                        color: '#555',
                                        border: 'solid 2px #FFF',
                                        borderRadius: '3px'
                                    }}
                                    onChange={vals => {
                                        this.setState({ fridayH: vals })
                                    }}
                                />
                            </div>
                            <div className="single-time">
                                <label htmlFor="idc">
                                    Salida
                            </label>
                                <TimeField
                                    value={this.state.fridayHO}
                                    style={{
                                        width: '80%',
                                        padding: '5px 8px',
                                        color: '#555',
                                        border: 'solid 2px #FFF',
                                        borderRadius: '3px'
                                    }}
                                    onChange={vals => {
                                        this.setState({ fridayHO: vals })
                                    }}
                                />
                            </div>

                        </div>
                    </div>
                    <div className="form-input__control-group time__wrapper">
                        <span>Sábado</span>
                        <div className="form-input__control__item times">
                            <div className="single-time">
                                <label htmlFor="idc">
                                    Entrada
                            </label>
                                <TimeField
                                    value={this.state.saturdayH}
                                    style={{
                                        width: '80%',
                                        padding: '5px 8px',
                                        color: '#555',
                                        border: 'solid 2px #FFF',
                                        borderRadius: '3px'
                                    }}
                                    onChange={vals => {
                                        this.setState({ saturdayH: vals })
                                    }}
                                />
                            </div>
                            <div className="single-time">
                                <label htmlFor="idc">
                                    Salida
                            </label>
                                <TimeField
                                    value={this.state.saturdayHO}
                                    style={{
                                        width: '80%',
                                        padding: '5px 8px',
                                        color: '#555',
                                        border: 'solid 2px #FFF',
                                        borderRadius: '3px'
                                    }}
                                    onChange={vals => {
                                        this.setState({ saturdayHO: vals })
                                    }}
                                />
                            </div>

                        </div>
                    </div>
                    <div className="form-input__control-group time__wrapper">
                        <span>Domingo</span>
                        <div className="form-input__control__item times">
                            <div className="single-time">
                                <label htmlFor="idc">
                                    Entrada
                            </label>
                                <TimeField
                                    value={this.state.sundayH}
                                    style={{
                                        width: '80%',
                                        padding: '5px 8px',
                                        color: '#555',
                                        border: 'solid 2px #FFF',
                                        borderRadius: '3px'
                                    }}
                                    onChange={vals => {
                                        this.setState({ sundayH: vals })
                                    }}
                                />
                            </div>
                            <div className="single-time">
                                <label htmlFor="idc">
                                    Salida
                            </label>
                                <TimeField
                                    value={this.state.sundayHO}
                                    style={{
                                        width: '80%',
                                        padding: '5px 8px',
                                        color: '#555',
                                        border: 'solid 2px #FFF',
                                        borderRadius: '3px'
                                    }}
                                    onChange={vals => {
                                        this.setState({ sundayHO: vals })
                                    }}
                                />
                            </div>

                        </div>
                    </div>
                </div>
                {this.state.showErrorMessage ? <div className="invalid-hours-message" >Por favor ingrese por lo menos un horario para continuar.</div> : false}

                <div>
                    < Button fill type="button" onClick={goBack}>
                        Volver
                        </Button>
                    < Button bsStyle="primary" fill type="submit">
                        Siguiente
                        </Button>
                </div>
            </AvForm >
        )
    }
}
