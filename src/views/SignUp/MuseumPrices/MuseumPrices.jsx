import React, { Component } from 'react'
import { AvForm, AvField } from 'availity-reactstrap-validation';
import Button from "components/CustomButton/CustomButton.jsx";
import NumberFormat from 'react-number-format';
export default class MuseumPrices extends Component {
    getFormattedPrice = (val, type) => {
        const formatterCr = new Intl.NumberFormat('es-CR', {
            style: 'currency',
            currency: 'CRC',
            minimumFractionDigits: 2
        })

        return formatterCr.format(val).slice(1);
    }
    state = {
        currency: [
            { name: '₡ CRC', val: 'crc' },
            { name: '$ USD', val: 'usd' },
            { name: '€ EUR', val: 'eur' }
        ],
        priceChildren: this.props.values.priceChildren,
        priceStudent: this.props.values.priceStudent,
        priceAdult: this.props.values.priceAdult,
        priceElder: this.props.values.priceElder,
    }
    getCurrencyOptions = () => {
        return this.state.currency.map(currency => (
            <option
                key={currency.val}
                value={currency.val} >
                {currency.name}
            </option>
        ))
    }


    render() {
        const { goNext, values, goBack } = this.props
        return (
            <AvForm
                className="login-wrapper__container__form museum-details__form"
                onValidSubmit={(e, vals) => {
                    goNext(vals, this.state)
                }}
                style={{ width: '90%' }}>
                <div className="price-container">
                    <div className="price-container__currency-options">
                        <AvField type="select" name="childCurrency" label="Moneda" required value={[values.childCurrency]} multiple={false}>
                            {this.getCurrencyOptions()}
                        </AvField>
                    </div>
                    <div className="price-container__currency-value">
                        <div className="my-form-group">
                            <label htmlFor="priceChildren">Precio de Niños</label>
                            <NumberFormat thousandSeparator={true} style={{ color: '#3f3f3f', padding: '0.5rem', border: '2px solid #FFF', borderRadius: '3px', }} value={this.state.priceChildren} onChange={ev => {
                                this.setState({ priceChildren: parseInt(ev.target.value.replace(',', '')) })
                            }} />
                        </div>

                    </div>
                </div>

                <div className="price-container">
                    <div className="price-container__currency-options">
                        <AvField type="select" name="studentCurrency" label="Moneda" required value={[values.studentCurrency]} multiple={false}>
                            {this.getCurrencyOptions()}
                        </AvField>
                    </div>
                    <div className="price-container__currency-value">
                        <div className="my-form-group">
                            <label htmlFor="priceChildren">Precio de Estudiantes</label>
                            <NumberFormat thousandSeparator={true} style={{ color: '#3f3f3f', padding: '0.5rem', border: '2px solid #FFF', borderRadius: '3px', }} value={this.state.priceStudent} onChange={ev => {
                                this.setState({ priceStudent: parseInt(ev.target.value.replace(',', '')) })
                            }} />
                        </div>
                    </div>
                </div>
                <div className="price-container">
                    <div className="price-container__currency-options">
                        <AvField type="select" name="adultCurrency" label="Moneda" required value={[values.adultCurrency]} multiple={false}>
                            {this.getCurrencyOptions()}

                        </AvField>
                    </div>
                    <div className="price-container__currency-value">
                        <div className="my-form-group">
                            <label htmlFor="priceChildren">Precio de Adultos</label>
                            <NumberFormat thousandSeparator={true} style={{ color: '#3f3f3f', border: '2px solid #FFF', borderRadius: '3px', padding: '0.5rem' }} value={this.state.priceAdult} onChange={ev => {
                                this.setState({ priceAdult: parseInt(ev.target.value.replace(',', '')) })
                            }} />
                        </div>
                    </div>
                </div>
                <div className="price-container">
                    <div className="price-container__currency-options">
                        <AvField type="select" name="elderCurrency" label="Moneda" required value={[values.elderCurrency]} multiple={false}>
                            {this.getCurrencyOptions()}
                        </AvField>
                    </div>
                    <div className="price-container__currency-value">
                        <div className="my-form-group">
                            <label htmlFor="priceChildren">Precio de Adultos Mayores</label>
                            <NumberFormat thousandSeparator={true} style={{ color: '#3f3f3f', border: '2px solid #FFF', borderRadius: '3px', padding: '0.5rem' }} value={this.state.priceElder} onChange={ev => {
                                this.setState({ priceElder: parseInt(ev.target.value.replace(',', '')) })
                            }} />
                        </div>
                    </div>
                </div>
                <div className="menu-form__buttons">
                    < Button fill type="button" onClick={goBack}>
                        Volver
                        </Button>
                    < Button bsStyle="primary" fill type="submit" disabled={isNaN(this.state.priceChildren) || isNaN(this.state.priceAdult) || isNaN(this.state.priceStudent) || isNaN(this.state.priceElder)}>
                        Siguiente
                        </Button>
                </div>
            </AvForm>
        )
    }
}
