import React, { Component } from 'react'
import Button from "components/CustomButton/CustomButton.jsx";
import { AvForm, AvField } from 'availity-reactstrap-validation';

import { Gmaps, Marker } from 'react-gmaps';

import '../SignUp.scss'
const params = { v: '3.exp', key: 'AIzaSyByGHOqa5T-MQI1_NA6uVfEfVUTSuXuxvo' };
export default class MuseumLocation extends Component {
    state = {
        lat: this.props.values.location.lat,
        lng: this.props.values.location.lng
    }
    onMapCreated(map) {
        map.setOptions({
            disableDefaultUI: true
        });
    }
    onDragEnd = (e) => {
        this.setState({ lat: e.latLng.lat(), lng: e.latLng.lng() })
    }
    onCloseClick = () => {
        console.log('onCloseClick');
    }
    onClick = (e) => {
        console.log('onClick', e);
        this.setState({ lat: e.latLng.lat(), lng: e.latLng.lng() })
    }
    render() {
        const { goNext, values, goBack } = this.props
        return (
            <div className="museum-details__wrapper">
                <Gmaps
                    onClick={this.onClick}
                    width={'600px'}
                    height={'300px'}
                    lat={this.state.lat}
                    lng={this.state.lng}
                    zoom={12}
                    loadingMessage={'Be happy'}
                    params={params}
                    onMapCreated={this.onMapCreated}>
                    <Marker
                        lat={this.state.lat}
                        lng={this.state.lng}
                        draggable={true}
                        onDragEnd={this.onDragEnd} />
                </Gmaps>
                <AvForm className="login-wrapper__container__form"
                    onValidSubmit={(e, vals) => {
                        goNext({ location: { lat: this.state.lat, lng: this.state.lng }, desc: vals })
                    }} style={{ width: '80%' }} >
                    <AvField name="museumLocationDescription" label="Dirección" errorMessage="Dirección inválida" type="textarea" validate={{
                        required: { value: true }
                    }}
                        rows="5"
                        value={values.description} />
                    <div>
                        < Button fill type="button" onClick={goBack}>
                            Volver
                    </Button>
                        < Button bsStyle="primary" fill type="submit" style={{ margin: '2rem' }}>
                            Siguiente
                    </Button>
                    </div>
                </AvForm>

            </div>
        )
    }
}
