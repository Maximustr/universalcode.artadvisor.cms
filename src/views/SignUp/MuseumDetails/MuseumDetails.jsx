import React from 'react';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import Button from "components/CustomButton/CustomButton.jsx";
import MuseumLogo from '../../../assets/img/Museum.svg'
import '../SignUp.scss'

class MuseumDetails extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            museumImage: props.values.museumImage,
        }
    }
    onImageChange = image => {
        this.setState({ museumImage: image })
    }
    onImgLoad({ target: img }) {
        // const height = img.offsetHeight
        // const width = img.offsetWidth
    }
    render() {
        const imageToShow = this.state.museumImage ? URL.createObjectURL(this.state.museumImage) : MuseumLogo
        const { goNext, values, goBack } = this.props
        return (
            <AvForm className="login-wrapper__container__form museum-details__form" onValidSubmit={(e, vals) => goNext(vals, this.state.museumImage)} style={{ width: '80%' }}  >
                <section className="museum-form__section">
                    <div className="museum-form__item">
                        <AvField name="museumName" label="Nombre del museo" errorMessage="Nombre inválido" type="text" validate={{
                            required: { value: true }
                        }}
                            value={values.museumName} />
                        <AvField name="museumDesc" label="Descripción del  museo" errorMessage="Descripción inválida" type="textarea" validate={{
                            required: { value: true }
                        }}
                            rows="3"
                            value={values.museumDesc} />
                        <AvField name="museumLink" label="Link del museo" errorMessage="Link inválido" type="test" validate={{
                            required: { value: true }
                        }}
                            value={values.museumLink} />
                        <AvField name="museumPhoneNumber" label="Número de contacto" errorMessage="Número de contacto inválido" type="text" validate={{
                            required: { value: true }
                        }}
                            value={values.museumPhoneNumber} />
                    </div>
                    <div className="museum-form__item">
                        <div className="image_controller" style={{ backgroundColor: !this.state.museumImage ? '#FFFFFF90' : null }}>
                            <img className="museum-image" onLoad={this.onImgLoad} src={imageToShow} alt="avatar" />
                            <div className="file-input-wrapper">
                                <button className="btn-file-input" type="button">
                                    Imagen de museo
						</button>
                                <input
                                    className="input-file"
                                    accept='image/*'
                                    type="file"
                                    name="file"
                                    onChange={ev => this.onImageChange(ev.target.files[0])}
                                />
                            </div>
                        </div>
                    </div>
                </section>
                <section className="museum-form__section">
                    <div className="menu-form__buttons">
                        <Button fill type="button" onClick={goBack}>
                            Volver
                    </Button>
                        < Button bsStyle="primary" fill type="submit" disabled={!this.state.museumImage} style={{ margin: '2rem' }}>
                            Siguiente
                    </Button>
                    </div>

                </section>
            </AvForm>
        );
    }

};

export default MuseumDetails;