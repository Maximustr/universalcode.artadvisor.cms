import React from 'react';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import Button from "components/CustomButton/CustomButton.jsx";
import '../SignUp.scss'

const UserDetails = ({ goNext, values }) => {
    var a = new RegExp('^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$')
    return (
        <AvForm className="login-wrapper__container__form" onValidSubmit={goNext} >
            <AvField name="email" type="email" errorMessage="Correo electronico no válido" label="Correo electrónico" validate={{
                required: { value: true },
            }}
                value={values.email}
            />
            <AvField name="password" label="Contraseña" errorMessage="Contraseña invalida" type="password" errorMessage='La contraseña debe ser de 4-16 caracteres' validate={{
                required: { value: true },
                minLength: { value: 4 },
                maxLength: { value: 16 }
            }
            }
                value={values.password}
            />
            <AvField name="confirmPassword" label="Confirmar contraseña" errorMessage="Contraseñas no coinciden" type="password" validate={{
                match: { value: 'password' },
                required: { value: true },
            }}
                value={values.confirmPassword} />
            < Button bsStyle="primary" fill type="submit">
                Siguiente
             </Button>
        </AvForm>
    );
};

export default UserDetails;