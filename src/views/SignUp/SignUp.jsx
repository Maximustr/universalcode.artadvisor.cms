import React, { Component } from 'react'
import './SignUp.scss'
import { Link } from 'react-router-dom'
import Button from "components/CustomButton/CustomButton.jsx";
import { Gmaps, Marker } from 'react-gmaps';
import UserDetails from './UserDetails/UserDetails';
import MuseumDetails from './MuseumDetails/MuseumDetails';
import MuseumLocation from './MuseumLocation/MuseumLocation';
import MuseumPrices from './MuseumPrices/MuseumPrices';
import MuseumSchedules from './MuseumSchedules/MuseumSchedules';
import { connect } from 'react-redux'
import { createUser } from '../../store/reducers';
import Spinner from '../../shared/Spinner/Spinner'


const params = { v: '3.exp', key: 'AIzaSyByGHOqa5T-MQI1_NA6uVfEfVUTSuXuxvo' };

class SignUp extends Component {
    constructor(props) {
        super(props)
        this.goBack = this.goBack.bind(this)
    }
    state = {
        needFullHeight: false,
        currentFormPage: 1,
        userDetails: {
            email: '',
            password: '',
            confirmPassword: ''
        },
        museumDetails: {
            museumName: '',
            museumDesc: '',
            museumLink: '',
            museumPhoneNumber: '',
            museumImage: null
        },
        location: {
            lat: 9.9325427,
            lng: -84.0795782
        },
        description: '',
        prices: {
            priceChildren: 0,
            priceAdult: 0,
            priceElder: 0,
            priceStudent: 0,
            studentCurrency: 'crc',
            childCurrency: 'crc',
            adultCurrency: 'crc',
            elderCurrency: 'crc'
        },
        time: {
            mondayH: '00:00',
            tuesdayH: '00:00',
            wednesayH: '00:00',
            thursdayH: '00:00',
            fridayH: '00:00',
            saturdayH: '00:00',
            sundayH: '00:00',


            mondayHO: '00:00',
            tuesdayHO: '00:00',
            wednesayHO: '00:00',
            thursdayHO: '00:00',
            fridayHO: '00:00',
            saturdayHO: '00:00',
            sundayHO: '00:00'

        }
    }
    resetValues = () => {
        this.setState({
            needFullHeight: false,
            currentFormPage: 1,
            userDetails: {
                email: '',
                password: '',
                confirmPassword: ''
            },
            museumDetails: {
                museumName: '',
                museumDesc: '',
                museumLink: '',
                museumPhoneNumber: '',
                museumImage: 0
            },
            location: {
                lat: 9.9325427,
                lng: -84.0795782
            },
            description: '',
            prices: {
                priceChildren: 0,
                priceAdult: 0,
                priceElder: 0,
                priceStudent: null,
                studentCurrency: 'crc',
                childCurrency: 'crc',
                adultCurrency: 'crc',
                elderCurrency: 'crc'
            },
            time: {
                mondayH: '00:00',
                tuesdayH: '00:00',
                wednesayH: '00:00',
                thursdayH: '00:00',
                fridayH: '00:00',
                saturdayH: '00:00',
                sundayH: '00:00',


                mondayHO: '00:00',
                tuesdayHO: '00:00',
                wednesayHO: '00:00',
                thursdayHO: '00:00',
                fridayHO: '00:00',
                saturdayHO: '00:00',
                sundayHO: '00:00'
            }
        })
    }
    createUser = () => {
        this.props.onCreateUser(this.state, (err) => {
            if (!err) {
                this.props.handleClick('tr', 'success', `Registro Exitoso!`, 'pe-7s-check')
                this.props.history.push('/login')
            } else {
                this.props.handleClick('tr', 'error', 'Error usuario ya tiene cuenta', 'pe-7s-close')
            }
        })
    }
    goNext = (e, vals) => {
        const newUserDetails = {
            email: vals.email,
            password: vals.password,
            confirmPassword: vals.confirmPassword
        }
        this.setState({ userDetails: newUserDetails, currentFormPage: this.state.currentFormPage + 1 })
    }
    goToMap = (vals, image) => {
        const museumDetails = {
            museumName: vals.museumName,
            museumDesc: vals.museumDesc,
            museumPhoneNumber: vals.museumPhoneNumber,
            museumLink: vals.museumLink,
            museumImage: image
        }
        this.setState({
            museumDetails: museumDetails,
            currentFormPage: this.state.currentFormPage + 1
        })
    }
    goToPrices = (values) => {
        this.setState({ location: values.location, description: values.desc.museumLocationDescription, currentFormPage: this.state.currentFormPage + 1 })
    }
    goToSchedules = (values, prices) => {
        const { childCurrency, adultCurrency, elderCurrency, studentCurrency } = values
        const newPrices = {
            priceChildren: prices.priceChildren,
            priceAdult: prices.priceAdult,
            priceElder: prices.priceElder,
            priceStudent: prices.priceStudent,
            childCurrency: Array.isArray(childCurrency) ? childCurrency[0] : childCurrency,
            adultCurrency: Array.isArray(adultCurrency) ? adultCurrency[0] : adultCurrency,
            elderCurrency: Array.isArray(elderCurrency) ? elderCurrency[0] : elderCurrency,
            studentCurrency: Array.isArray(studentCurrency) ? studentCurrency[0] : studentCurrency
        }
        this.setState({ prices: newPrices, currentFormPage: this.state.currentFormPage + 1 })
    }
    goToSummary = (vals) => {
        this.setState({ time: vals, currentFormPage: this.state.currentFormPage + 1 })
    }
    goBack = () => {
        this.setState({ currentFormPage: this.state.currentFormPage - 1 })
    }
    onMapCreated(map) {
        map.setOptions({
            disableDefaultUI: true
        });
    }
    getFormattedPrice = (val) => {
        const formatterCr = new Intl.NumberFormat('es-CR', {
            style: 'currency',
            currency: 'CRC',
            minimumFractionDigits: 2
        })
        return formatterCr.format(val).slice(1);
    }
    getFormPage = () => {
        switch (this.state.currentFormPage) {
            case 1:
                return (
                    <div className={`sign-up__wrapper__color__container  full-height`}>
                        <div className="sign-up__wrapper__container">
                            <div className="full-container">
                                <h1>Registro</h1>
                                <h2 style={{ marginTop: '0' }}>{this.getStep(this.state.currentFormPage)}</h2>
                                <UserDetails values={this.state.userDetails} goNext={this.goNext} />
                                <Link to="/forgot-password" className="effect-underline">Recuperar contraseña</Link>

                                <Link to="/login" className="effect-underline">Ya tengo cuenta</Link>
                            </div>
                        </div>
                    </div>
                )
            case 2:
                return (
                    <div className={`sign-up__wrapper__color__container full-height `}>
                        <div className="sign-up__wrapper__container">
                            <div className="full-container">
                                <h1>Registro</h1>
                                <h2 style={{ marginTop: '0' }}>{this.getStep(this.state.currentFormPage)}</h2>
                                <MuseumDetails goBack={this.goBack} values={this.state.museumDetails} goNext={this.goToMap} />
                                <Link to="/forgot-password" className="effect-underline">Recuperar contraseña</Link>

                                <Link to="/login" className="effect-underline">Ya tengo cuenta</Link>
                            </div>
                        </div>
                    </div>
                )
            case 3:
                return (
                    <div className={`sign-up__wrapper__color__container full-height `}>
                        <div className="sign-up__wrapper__container">
                            <div className="full-container">
                                <h1>Registro</h1>
                                <h2 style={{ marginTop: '0' }}>{this.getStep(this.state.currentFormPage)}</h2>
                                <MuseumLocation goBack={this.goBack} values={{ location: this.state.location, description: this.state.description }} goNext={this.goToPrices} />
                                <Link to="/forgot-password" className="effect-underline">Recuperar contraseña</Link>

                                <Link to="/login" className="effect-underline">Ya tengo cuenta</Link>
                            </div>
                        </div>
                    </div>
                )
            case 4:
                return (
                    <div className={`sign-up__wrapper__color__container full-height`}  >
                        <div className="sign-up__wrapper__container" style={{ width: '70%' }}>
                            <div className="full-container">
                                <h1>Registro</h1>
                                <h2 style={{ marginTop: '0' }}>{this.getStep(this.state.currentFormPage)}</h2>
                                <MuseumPrices goBack={this.goBack} values={this.state.prices} goNext={this.goToSchedules} />
                                <Link to="/forgot-password" className="effect-underline">Recuperar contraseña</Link>

                                <Link to="/login" className="effect-underline">Ya tengo cuenta</Link>
                            </div>
                        </div>
                    </ div >
                )
            case 5:
                return (
                    <div className={`sign-up__wrapper__color__container`} style={{ height: '100vh' }}>
                        <div className="sign-up__wrapper__container" style={{ width: '50%' }}>
                            <div className="full-container">
                                <h1>Registro</h1>
                                <h2 style={{ marginTop: '0' }}>{this.getStep(this.state.currentFormPage)}</h2>
                                <MuseumSchedules goBack={this.goBack} time={this.state.time} goNext={this.goToSummary} />
                                <Link to="/forgot-password" className="effect-underline">Recuperar contraseña</Link>

                                <Link to="/login" className="effect-underline">Ya tengo cuenta</Link>
                            </div>
                        </div>
                    </div>
                )
            case 6:
                return (
                    <div className={`sign-up__wrapper__color__container`}>
                        <div className="sign-up__wrapper__container" style={{ width: '50%' }}>
                            <div className="full-container">
                                <h1>{this.props.waitingForResponceUser ? 'Procesando' : 'Información'}</h1>
                                {this.props.waitingForResponceUser ? <Spinner isWhite /> : <React.Fragment>
                                    <span style={{ marginTop: '0' }}>
                                        Correo electónico: {this.state.userDetails.email}
                                    </span>
                                    <div className="dividor-horizontal" />
                                    <span>Nombre de Museo: {this.state.museumDetails.museumName}</span>
                                    <span>Número de Museo: {this.state.museumDetails.museumPhoneNumber}</span>
                                    <span>Descripción</span>
                                    <p>{this.state.museumDetails.museumDesc}</p>
                                    <span>Sitio web de museo: {this.state.museumDetails.museumLink}</span>
                                    {this.state.museumDetails.museumImage ? <img className="summary-img" src={URL.createObjectURL(this.state.museumDetails.museumImage)} /> : null}
                                    <div className="dividor-horizontal" />
                                    <span>
                                        Dirección:
                                </span>
                                    <p>{this.state.description}</p>
                                    <Gmaps
                                        width={'500px'}
                                        height={'200px'}
                                        lat={this.state.location.lat}
                                        lng={this.state.location.lng}
                                        zoom={12}
                                        loadingMessage={'Be happy'}
                                        params={params}
                                        onMapCreated={this.onMapCreated}>
                                        <Marker
                                            lat={this.state.location.lat}
                                            lng={this.state.location.lng}
                                            draggable={false} />
                                    </Gmaps>
                                    <div className="dividor-horizontal" />
                                    <span>Precios</span>
                                    <div className="price-summary">
                                        <span>Estudiante: {this.getCurrency(this.state.prices.studentCurrency)} {this.getFormattedPrice(this.state.prices.priceStudent)}</span><div className="divider-vertical" />
                                        <span>Adulto: {this.getCurrency(this.state.prices.adultCurrency)} {this.getFormattedPrice(this.state.prices.priceAdult)}</span><div className="divider-vertical" />
                                        <span>Niños: {this.getCurrency(this.state.prices.childCurrency)} {this.getFormattedPrice(this.state.prices.priceChildren)}</span><div className="divider-vertical" />
                                        <span>Adultos Mayores: {this.getCurrency(this.state.prices.elderCurrency)} {this.getFormattedPrice(this.state.prices.priceElder)}</span>
                                    </div>
                                    <div className="dividor-horizontal" />
                                    <span>Horario</span>
                                    <div className="price-summary">
                                        <span>Lunes: {this.getPmOrAm(this.state.time.mondayH, this.state.time.mondayHO)} </span><div className="divider-vertical" />
                                        <span>Martes: {this.getPmOrAm(this.state.time.tuesdayH, this.state.time.tuesdayHO)} </span><div className="divider-vertical" />
                                        <span>Miércoles: {this.getPmOrAm(this.state.time.wednesayH, this.state.time.wednesayHO)} </span><div className="divider-vertical" />
                                        <span>Jueves: {this.getPmOrAm(this.state.time.thursdayH, this.state.time.thursdayHO)} </span><div className="divider-vertical" />
                                        <span>Viernes:{this.getPmOrAm(this.state.time.fridayH, this.state.time.fridayHO)} </span><div className="divider-vertical" />
                                        <span>Sábado: {this.getPmOrAm(this.state.time.saturdayH, this.state.time.saturdayHO)} </span><div className="divider-vertical" />
                                        <span>Domingo: {this.getPmOrAm(this.state.time.sundayH, this.state.time.sundayHO)} </span>
                                    </div>
                                    <div>
                                        < Button fill type="button" onClick={() => this.setState({ currentFormPage: this.state.currentFormPage - 1 })}>
                                            Volver
                                        </Button>
                                        < Button fill type="button" bsStyle="primary" onClick={() => this.createUser()}>
                                            Registrar
                                        </Button>
                                    </div>
                                </React.Fragment>}
                            </div>
                        </div>
                    </div>
                )
            default:
                return null
        }
    }
    getPmOrAm = (val1, val2) => {
        if (val1 === "00:00" && val2 === "00:00") {
            return 'Cerrado'
        }
        return `${val1} am - ${val2} pm`
    }
    getCurrency = (val) => {
        switch (val) {
            case 'crc':
                return '₡ CRC'
            case 'usd':
                return '$ USD'
            case 'eur':
                return '€ EUR'
            default:
                return
        }
    }
    getStep = (number) => {
        let step = `Paso: ${number}`
        switch (number) {
            case 1:
                return `${step}/5 Información de usuario`
            case 2:
                return `${step}/5 Información de Museo`
            case 3:
                return `${step}/5 Ubicación de Museo`
            case 4:
                return `${step}/5 Precios de Museo`
            case 5:
                return `${step}/5 Horarios de Museo`
            default:
                return null
        }
    }
    render() {
        return (
            <div className="sign-up__wrapper" style={this.props.waitingForResponceUser ? { height: '100%' } : null}>
                {this.getFormPage()}
            </div>
        )
    }
}

const mapStateToProps = ({ userReducer }) => ({
    waitingForResponceUser: userReducer.waitingforResponce,
})

const mapDispatchToProps = dispatch => ({
    onCreateUser: (vals, func) => dispatch(createUser(vals, func))
})
export default connect(mapStateToProps, mapDispatchToProps)(SignUp)
