import React, { Component } from 'react'
import { connect } from 'react-redux'
import { AvForm, AvField } from 'availity-reactstrap-validation';
import Button from "components/CustomButton/CustomButton.jsx";
import './TranslationUpdate.scss'
import {
    fetchArtPieces,
    fetchAllLanguages,
    createTranslation,
    resetValues,
    fetchAllTranstaltions, fetchAllAvaliableLanguajes
} from '../../../store/reducers';


class TranslationUpdate extends Component {
    state = {
        translationToUpdate: {},
        translationToUpdateId: null,
        languageSelected: null,
        artPieceSelected: null
    }
    componentDidMount() {
        this.props.onFetchArtPieces({ museumId: this.props.museumId })
        this.setState({ translationToUpdateId: this.props.match.params.id })
        if (this.props.match.params.id) {
            this.props.onFetchAllLangujes()
        } else {
            this.props.onFetchAllLangujes()
        }
        this.props.onFetchAllTranslations({ museumId: this.props.museumId })
    }
    onGoBack = () => {
        this.props.history.goBack()
    }
    onCreate = (e, values) => {
        const { translationToUpdateId } = this.state
        const translation = {
            museumId: this.props.museumId,
            artPieceId: values.art_piece[0],
            language: values.language[0],
            Translation: values.translation,
            id: translationToUpdateId
        }
        this.props.onCreateTranslation(translation, (err) => {
            if (!err) {
                this.props.handleClick('tr', 'success', `Traducción ${translationToUpdateId ? 'Actualizada' : 'Creada'}!`, 'pe-7s-check')
                this.onGoBack()
            } else {
                this.props.handleClick('tr', 'error', err, 'pe-7s-attention')
            }
            this.props.onResetValues()
        })
    }

    getOptions = (list, selected) => {
        return list.map(val => <option
            key={val.id}
            value={val.id}
            selected={val.id === selected}>
            {val.name}
        </option>)
    }
    render() {
        const { artPieces, languages, translations } = this.props
        const { translationToUpdateId } = this.state
        let translationToUpdate = {}
        if (translationToUpdateId && translations.length !== 0) {
            translationToUpdate = translations.find(a => a.id === this.props.match.params.id)
        }
        return (
            <div className="translation-update__wrapper">
                <h2>{!translationToUpdateId ? 'Crear' : 'Actualizar'} Traducción</h2>
                <AvForm onValidSubmit={this.onCreate} className="translation-update__wrapper__form">
                    <AvField type="select" name="art_piece" label="Obra de arte" multiple required defaultValue={
                        translationToUpdate.artPieceId ? [translationToUpdate.artPieceId] : null
                    } errorMessage="Obra inválida">
                        {this.getOptions(artPieces, translationToUpdate.artPieceId)}
                    </AvField>
                    <AvField type="select" name="language" label="Lenguaje" multiple required
                        defaultValue={
                            translationToUpdate.language ? [translationToUpdate.language] : null
                        } errorMessage="Lenguaje inválido">
                        {this.getOptions(languages, translationToUpdate.language)}
                    </AvField>
                    <AvField id="orders-details" type="textarea" value={translationToUpdate.Translation || ''}
                        rows={5}
                        name="translation" label="Traducción" required
                        errorMessage="Traducción inválida" />
                    <div>
                        <Button bsStyle="secondary" fill type="button" onClick={() => this.onGoBack()}>
                            Atrás
            </Button>
                        <Button bsStyle="info" fill type="submit" >
                            {!translationToUpdateId ? 'Crear' : 'Actualizar'}
                        </Button>
                    </div>
                </AvForm>
            </div>
        )
    }
}

const mapStateToProps = ({ artPiecesReducer, languageReducer, translationReducer, userReducer }) => ({
    waitingForResponce: artPiecesReducer.waitingForResponce,
    artPieces: artPiecesReducer.artPieces,
    error: artPiecesReducer.error,
    languages: languageReducer.languages,
    added: translationReducer.added,
    translations: translationReducer.translations,
    museumId: userReducer.userMuseum.id
})

const mapDispatchToProps = dispatch => ({
    onFetchArtPieces: (val) => dispatch(fetchArtPieces(val)),
    onFetchAllLangujes: () => dispatch(fetchAllLanguages()),
    onCreateTranslation: (val, notifyFunc) => dispatch(createTranslation(val, notifyFunc)),
    onResetValues: () => dispatch(resetValues()),
    onFetchAllTranslations: val => dispatch(fetchAllTranstaltions(val)),
    onFetchAvaliableLanguages: id => dispatch(fetchAllAvaliableLanguajes(id))
})

export default
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(TranslationUpdate)

