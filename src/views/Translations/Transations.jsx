import React from 'react';
import { Switch, Route } from 'react-router-dom';
import TranslationUpdate from './Translations-Update/TranslationsUpdate';
import TranslationsList from './Translations-List/TranslationsList';

class Translations extends React.Component {
  render() {
    return (
      <>
        <Switch {...this.props}>
          <Route exact path={`${this.props.match.url}`} component={TranslationsList} />
          <Route
            exact
            path={`${this.props.match.url}/new`}
            render={(props) => (<TranslationUpdate {...props} handleClick={this.props.handleClick} />)} />
          <Route
            exact
            path={`${this.props.match.url}/:id`}
            render={(props) => (<TranslationUpdate {...props} handleClick={this.props.handleClick} />)} />
        </Switch>
      </>
    )
  }

}
export default Translations;