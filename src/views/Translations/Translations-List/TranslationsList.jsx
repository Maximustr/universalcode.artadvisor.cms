import React, { Component } from 'react'
import { Grid, Row, Col, Table } from "react-bootstrap";
import { thTranslations } from '../../../variables/Variables'
import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import { connect } from 'react-redux'
import { fetchArtPieces, fetchAllTranstaltions, fetchAllLanguages, removeTranslation } from '../../../store/reducers';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import './TranslationList.scss'
import Spinner from '../../../shared/Spinner/Spinner';
const MySwal = withReactContent(Swal)

class TranslationsList extends Component {

    componentDidMount() {
        const values = { museumId: this.props.museum.id }
        this.props.onFetchAllTranslations(values)
        this.props.onFetchArtPieces(values)
        this.props.onFetchLanguages()
    }
    onDeleteHandler = (id) => {
        const { artPieces, languages, translations } = this.props
        const item = this.getItemFromListById(id, translations)
        const artPiece = this.getItemFromListById(item.artPieceId, artPieces)
        const language = this.getItemFromListById(item.language, languages)
        MySwal.fire({
            title: '¿Esta seguro?',
            text: `¿Seguro que desea eliminar la traducción en ${language.name} de la obra ${artPiece.name}?`,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#EF8E84',
            cancelButtonColor: '#878787',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Aceptar'
        }).then((result) => {
            if (result.value) {
                this.props.onRemoveTranslations(id, (err) => {
                    if (!err) {
                        Swal.fire(
                            'Eliminada!',
                            'Traducción eliminada',
                            'success'
                        )
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: 'Oops...',
                            text: 'Lo sentimos intentalo ma tarde!',
                        })
                    }
                })

            }
        })
    }

    getItemFromListById = (id, list) => {
        return list.find(a => a.id === id)
    }

    getRow = item => {
        const { artPieces, languages } = this.props
        const artPiece = this.getItemFromListById(item.artPieceId, artPieces)
        const language = this.getItemFromListById(item.language, languages)
        return (
            <tr key={item.id}>
                <td>{artPiece ? artPiece.name : null}</td>
                <td>{language ? language.name : null}</td>

                <td >{item.Translation.substring(0, 44)}...</td>
                <td>
                    <Button bsStyle="danger" fill type="button" onClick={() => this.onDeleteHandler(item.id)}>
                        Eliminar
          </Button>
                </td>
                <td>
                    <Button bsStyle="info" fill type="submit" onClick={() => this.onNavigateToUpdate(item.id)}>
                        Actualizar
          </Button>
                </td>
            </tr>
        )
    }
    onNavigateToCreate = () => {
        const { match } = this.props
        this.props.history.push(`${match.url}/new`)
    }
    onNavigateToUpdate = (id) => {
        const { match } = this.props
        this.props.history.push(`${match.url}/${id}`)
    }
    onFilter = (vtoFilter) => {
        const values = { museumId: this.props.museum.id }
        if (vtoFilter !== 'allPieces') {
            values.pieceId = vtoFilter
        }
        this.props.onFetchAllTranslations(values)
    }
    getOpts = (list) => {
        const opts = [
            <option key="allPieces" value={"allPieces"}>Todas las obras</option>,
            ...list.map(item => <option value={item.id} key={item.id}>{item.name}</option>)
        ]
        return opts;
    }
    getContent = () => {
        const { waitingForResponceArt, waitingForResponceTranslations, translations, artPieces } = this.props
        if (waitingForResponceArt || waitingForResponceTranslations) {
            return <Spinner />
        } else {
            if (translations.length === 0) {
                return (
                    <div className="empty-list-text">
                        <h1>No hay traducciones aún.</h1>
                    </div>
                )
            } else if (artPieces.length === 0) {
                return (
                    <div className="empty-list-text">
                        <h1>No hay Obras aún.</h1>
                    </div>
                )
            } else {
                return (
                    <Table hover>
                        <thead>
                            <tr>
                                {thTranslations.map((prop, key) => {
                                    return <th key={key}>{prop}</th>;
                                })}
                            </tr>
                        </thead>
                        <tbody>
                            {translations.map((prop, key) => {
                                return (
                                    this.getRow(prop)
                                );
                            })}
                        </tbody>
                    </Table>
                )
            }
        }
    }

    render() {
        const { artPieces } = this.props
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Card
                                title="Traducción"
                                category="Traducciones de obras"
                                ctTableFullWidth
                                ctTableResponsive
                                content={
                                    <div style={{ padding: '1rem' }}>
                                        <div className="constrols-wrapper">
                                            <div className="constrols-wrapper__item button-wrapper">
                                                <Button bsStyle="success" fill type="submit" onClick={this.onNavigateToCreate} style={{ margin: 'none', width: '164px', height: '41px' }}>
                                                    Crear traducción
                      </Button>
                                            </div>
                                            <div className="constrols-wrapper__item">
                                                <AvForm >
                                                    <AvField
                                                        style={{ margin: 'none', width: '164px', height: '41px' }}
                                                        type="select"
                                                        name="select"
                                                        onChange={(ev) => { this.onFilter(ev.target.value) }}>
                                                        {this.getOpts(artPieces)}
                                                    </AvField>
                                                </AvForm>
                                            </div>
                                        </div>
                                        {this.getContent()}

                                    </div>
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}
const mapStateToProps = ({ artPiecesReducer, translationReducer, languageReducer, userReducer }) => ({
    waitingForResponceArt: artPiecesReducer.waitingForResponce,
    waitingForResponceTranslations: translationReducer.waitingForResponce,
    artPieces: artPiecesReducer.artPieces,
    error: artPiecesReducer.error,
    translations: translationReducer.translations,
    languages: languageReducer.languages,
    museum: userReducer.userMuseum
})

const mapDispatchToProps = dispatch => ({
    onFetchArtPieces: (val) => dispatch(fetchArtPieces(val)),
    onFetchAllTranslations: val => dispatch(fetchAllTranstaltions(val)),
    onFetchLanguages: () => dispatch(fetchAllLanguages()),
    onRemoveTranslations: (id, func) => dispatch(removeTranslation(id, func))
})

export default
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(TranslationsList)


