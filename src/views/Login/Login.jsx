import React, { Component } from 'react'
import { AvForm, AvField } from 'availity-reactstrap-validation';
import { connect } from 'react-redux'
import Button from "components/CustomButton/CustomButton.jsx";
import './Login.scss'
import { style } from "variables/Variables.jsx";

import NotificationSystem from "react-notification-system";

import { Link } from 'react-router-dom'
import { loginUser, fetchUser, createUser } from '../../store/reducers';
import Spinner from '../../shared/Spinner/Spinner'

class Login extends Component {
    constructor(props) {

        super(props)
        this.onExecLoin = this.onExecLoin.bind(this)
        this.onErrorFuc = this.onErrorFuc.bind(this)
        this.onSuccessFunc = this.onSuccessFunc.bind(this)
    }
    state = {
        _notificationSystem: null
    }
    handleNotificationClick = (position, level, text, icon = 'pe-7s-gift') => {
        this.setState({ _notificationSystem: this.refs.notificationSystem });
        this.refs.notificationSystem.addNotification({
            title: <span data-notify="icon" className={icon} />,
            message: (
                <div>
                    {text}
                </div>
            ),
            level: level,
            position: position,
            autoDismiss: 3
        });
    }

    onExecLoin = (e, values) => {
        this.props.onLogin(values, this.onSuccessFunc, this.onErrorFuc)
    }
    onSuccessFunc = uid => {
        this.props.onFetchUser(uid)
    }
    onErrorFuc = err => {
        this.handleNotificationClick('tr', 'error', err, 'pe-7s-close')
    }
    render() {
        const { waitingForResponce, waitingForResponceUser } = this.props
        return (
            <div className="login-wrapper">
                <NotificationSystem ref="notificationSystem" style={style} />

                <div className="login-color__wrapper">
                    <div className="login-wrapper__container">
                        <h1>Iniciar sesión</h1>
                        {waitingForResponce || waitingForResponceUser ?
                            (<Spinner isWhite />) :
                            (
                                <AvForm className="login-wrapper__container__form" onValidSubmit={this.onExecLoin}>
                                    <AvField name="email" type="email" errorMessage="Correo inválido" label="Correo" validate={{
                                        required: { value: true },
                                    }} />
                                    <AvField name="password" label="Contraseña" errorMessage="Contraseña inválida" type="password" validate={{
                                        required: { value: true }
                                    }} />
                                    < Button bsStyle="primary" fill type="submit">
                                        Iniciar sesión
                        </Button>
                                </AvForm>
                            )}
                        <Link to="/forgot-password" className="effect-underline">Recuperar contraseña</Link>
                        <Link to="/signup" className="effect-underline">No tengo cuenta</Link>
                    </div>
                </div>
            </div >
        )
    }
}

const mapStateToProps = ({ authReducer, userReducer }) => ({
    waitingForResponce: authReducer.waitingForResponce,
    waitingForResponceUser: userReducer.waitingforResponce,
    error: authReducer.error,
    uuid: authReducer.uuid,
})

const mapDispatchToProps = dispatch => ({
    onLogin: (val, successFunc, errFunc) => dispatch(loginUser(val, successFunc, errFunc)),
    onFetchUser: (uid) => dispatch(fetchUser(uid)),
    onCreateUser: (vals) => dispatch(createUser(vals))
})
export default connect(mapStateToProps, mapDispatchToProps)(Login)
