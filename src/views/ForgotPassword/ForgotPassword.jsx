import React, { Component } from 'react'
import { AvForm, AvField } from 'availity-reactstrap-validation';
import { connect } from 'react-redux'
import Button from "components/CustomButton/CustomButton.jsx";
import './ForgotPassword.scss'
import { Link } from 'react-router-dom'
import { forgotPassword } from '../../store/reducers';
import Spinner from '../../shared/Spinner/Spinner'

class ForgotPassword extends Component {
    constructor(props) {
        super(props)
        this.onExecLoin = this.onExecLoin.bind(this)
        this.onErrorFuc = this.onErrorFuc.bind(this)
        this.onSuccessFunc = this.onSuccessFunc.bind(this)
    }

    onExecLoin = (e, values) => {
        this.props.onForgotPassword(values.email, this.onSuccessFunc, this.onErrorFuc)
    }
    onSuccessFunc = () => {
        this.props.handleClick('tr', 'success', 'Correo enviado', 'pe-7s-mail')
    }
    onErrorFuc = err => {
        this.props.handleClick('tr', 'error', err, 'pe-7s-close')
    }
    render() {
        const { waitingForResponce } = this.props
        return (
            <div className="forgot-pass-wrapper">
                <div className="login-color__wrapper">
                    <div className="login-wrapper__container">
                        <h1>Recordar Contraseña</h1>
                        {waitingForResponce ?
                            (<Spinner isWhite />) :
                            (
                                <AvForm className="login-wrapper__container__form" onValidSubmit={this.onExecLoin}>
                                    <AvField name="email" type="text" errorMessage="Correo inválido" label="Correo" validate={{
                                        required: { value: true },
                                    }} />

                                    < Button bsStyle="primary" fill type="submit">
                                        Enviar Correo
                                    </Button>
                                </AvForm>
                            )}

                        <Link to="/login" className="effect-underline">Iniciar Sesión</Link>
                        <Link to="/signup" className="effect-underline">No tengo cuenta</Link>
                    </div>
                </div>
            </div >
        )
    }
}

const mapStateToProps = ({ authReducer }) => ({
    waitingForResponce: authReducer.waitingForResponce,
    error: authReducer.error,
})

const mapDispatchToProps = dispatch => ({
    onForgotPassword: (val, successFunc, errFunc) => dispatch(forgotPassword(val, successFunc, errFunc)),
})
export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword)
