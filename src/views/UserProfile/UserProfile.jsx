import React, { Component } from "react";
import {
    Grid,
    Row,
    Col,

} from "react-bootstrap";

import { Card } from "components/Card/Card.jsx";

import { FormInputs } from "components/FormInputs/FormInputs.jsx";

import './user.scss'
import AvForm from "availity-reactstrap-validation/lib/AvForm";

class UserProfile extends Component {
    render() {
        return (
            <div className="user-list__wrapper">
                <Grid fluid>
                    <Row>
                        <Col md={8}>
                            <Card
                                title="Perfil"
                                content={
                                    <AvForm className="user-list__wrapper__form">
                                        <FormInputs
                                            ncols={["col-md-5", "col-md-3", "col-md-4"]}
                                            proprieties={[
                                                {
                                                    label: "Correo electrónico",
                                                    type: "email",
                                                    bsClass: "form-control",
                                                    placeholder: "cata@gmail.com",
                                                    defaultValue: "",
                                                    disabled: true
                                                },
                                                {
                                                    label: "Nombre",
                                                    type: "text",
                                                    bsClass: "form-control",
                                                    placeholder: "Catalina",
                                                    defaultValue: ""
                                                },
                                                {
                                                    label: "Apellido",
                                                    type: "text",
                                                    bsClass: "form-control",
                                                    placeholder: "Valenciano",
                                                    defaultValue: ""
                                                }
                                            ]}
                                        />
                                    </AvForm>
                                }
                            />
                        </Col>
                    </Row>
                </Grid>>
      </div>
        );
    }
}

export default UserProfile;
