import React, { Component } from 'react'
import { connect } from 'react-redux'
import { AvForm, AvField, } from 'availity-reactstrap-validation';
import Button from "components/CustomButton/CustomButton.jsx";
import './user.scss'
//import { addUser,removeUser,updateUser } from '../../store/reducers';
// import {  } from '../../../store/reducers';

class CreateUser extends Component {
    state = {
        userToUpdate: null,
    }
    componentDidMount() {
        // this.props.onFetchUsers()
    }
    onCreate = (e, values) => {
        debugger
    }
    onAddUserFunction = (e, values) => {
        const { userToUpdate } = this.state
        if (userToUpdate) {
            debugger
            const user = {
                ...values,
                id: userToUpdate.id
            }
            this.props.onUpdateUser(user)
        } else {
            this.props.onAddUser(values)
        }
        this.setState({ updatingId: null })
    }
    onDeleteUser = id => {
        this.props.onRemoveUser({ id })
    }
    onSetUserToUpdate = user => {
        this.setState({ userToUpdate: user })
    }
    render() {

        return (
            <div className="user-create__wrapper">
                <h2>Registrar usuario</h2>

                <AvForm onValidSubmit={this.onAddUserFunction}
                    onInvalidSubmit={this.handleInvalidSubmit} className="user-create__wrapper__form">
                    <div className="flex-container ">
                        <AvField name="name" label="Nombre:" placeholder="Catalina" type="text" validate={{
                            required: { value: true, errorMessage: 'Por favor completar el campo' },
                        }} />
                        <AvField name="lastName" label="Apellido:" placeholder="Valenciano" type="text" validate={{
                            required: { value: true, errorMessage: 'Por favor completar el campo' },
                        }} />
                    </div>
                    <div className="flex-container ">
                        <AvField name="email" label="Correo electrónico" placeholder="cata@gmail.com" type="text" validate={{
                            required: { value: true, email: true, errorMessage: 'Por favor completar el campo' },
                        }} />
                    </div>
                    <div className="flex-container ">
                        <Button bsStyle="danger" fill type="submit" pullLeft>
                            Atras
              </Button>
                        <Button bsStyle="success" fill type="submit" pullRight>
                            Crear registro
              </Button>
                    </div>
                </AvForm>
            </div>
        )
    }
}

const mapStateToProps = ({ userReducer }) => ({
    users: userReducer.users,
    waitingforResponce: userReducer.waitingforResponce,
    error: userReducer.error
})
const mapDispatchToProps = dispatch => ({
    //   onAddUser: user => dispatch(addUser(user)),
    //   onUpdateUser: user => dispatch(updateUser(user)),
    //   onRemoveUser: user => dispatch(removeUser(user))
})
export default
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(CreateUser)
