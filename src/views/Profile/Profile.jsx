import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import Button from "components/CustomButton/CustomButton.jsx";
import { Gmaps, Marker } from 'react-gmaps';
import Spinner from '../../shared/Spinner/Spinner'
import { fetchMuseuminfo } from '../../store/reducers';


const params = { v: '3.exp', key: 'AIzaSyByGHOqa5T-MQI1_NA6uVfEfVUTSuXuxvo' };

class Profile extends Component {
    componentDidMount() {
        this.props.onFetchMuseumInfo(this.props.currentUser.id)
    }
    state = {
        needFullHeight: false,
        currentFormPage: 1,
        userDetails: {
            email: '',
            password: '',
            confirmPassword: ''
        },
        museumDetails: {
            museumName: '',
            museumDesc: '',
            museumLink: '',
            museumPhoneNumber: '',
            museumImage: null
        },
        location: {
            lat: 9.9325427,
            lng: -84.0795782
        },
        description: '',
        prices: {
            priceChildren: 5000,
            priceAdult: 5000,
            priceElder: 5000,
            priceStudent: 5000,
            studentCurrency: 'crc',
            childCurrency: 'crc',
            adultCurrency: 'crc',
            elderCurrency: 'crc'
        },
        time: {
            mondayH: '00:00',
            tuesdayH: '00:00',
            wednesayH: '00:00',
            thursdayH: '00:00',
            fridayH: '00:00',
            saturdayH: '00:00',
            sundayH: '00:00'
        }
    }
    getPmOrAm = (val) => {
        if (val === "00:00") {
            return 'Cerrado'
        }
        return `${val} ${parseInt(val.substr(0, 2)) > 12 ? 'pm' : 'am'}`
    }
    onMapCreated(map) {
        map.setOptions({
            disableDefaultUI: true
        });
    }
    getCurrency = (val) => {
        switch (val) {
            case 'crc':
                return '₡ CRC'
            case 'usd':
                return '$ USD'
            case 'eur':
                return '€ EUR'
            default:
                return
        }
    }
    getFormattedPrice = (val) => {
        const formatterCr = new Intl.NumberFormat('es-CR', {
            style: 'currency',
            currency: 'CRC',
            minimumFractionDigits: 2
        })
        return formatterCr.format(val).slice(1);
    }
    render() {
        return (
            <div className="profile__full-wrapper">
                <div className="full-container" style={{ backgroundColor: '#FFF', color: '#000' }}>
                    <h1>Información</h1>
                    {this.props.waitingForResponce ? <Spinner /> : <React.Fragment>
                        <span style={{ marginTop: '0' }}>
                            Correo Electrónico: {this.props.currentUser.email}
                        </span>
                        <div className="dividor-horizontal" style={{ backgroundColor: '#000' }} />
                        <span>Nombre del Museo: {this.props.museum.name}</span>
                        <span>Número del Museo: {this.props.museum.phoneNumber}</span>
                        <span>Descripción</span>
                        <p>{this.props.museum.description}</p>
                        <span>Sitio web: {this.props.museum.link}</span>
                        <img className="summary-img" src={this.props.museum.img} />
                        <div className="dividor-horizontal" style={{ backgroundColor: '#000' }} />
                        <h2>
                            Dirección:
                                </h2>
                        <p>{this.props.museum.address}</p>
                        <Gmaps
                            width={'500px'}
                            height={'200px'}
                            lat={this.props.museum.latitud}
                            lng={this.props.museum.longitud}
                            zoom={12}
                            loadingMessage={'Be happy'}
                            params={params}
                            onMapCreated={this.onMapCreated}>
                            <Marker
                                lat={this.props.museum.latitud}
                                lng={this.props.museum.longitud}
                                draggable={false} />
                        </Gmaps>
                        <div className="dividor-horizontal" style={{ backgroundColor: '#000' }} />
                        <h2>Precios</h2>
                        <div className="price-summary">
                            <span>Estudiante: {this.getCurrency(this.props.museum.prices.studentCurrency)} {this.getFormattedPrice(this.props.museum.prices.priceStudent)}</span><div className="divider-vertical" style={{ backgroundColor: '#000' }} />
                            <span>Adulto: {this.getCurrency(this.props.museum.prices.adultCurrency)} {this.getFormattedPrice(this.props.museum.prices.priceAdult)}</span><div className="divider-vertical" style={{ backgroundColor: '#000' }} />
                            <span>Niños: {this.getCurrency(this.props.museum.prices.childCurrency)} {this.getFormattedPrice(this.props.museum.prices.priceChildren)}</span><div className="divider-vertical" style={{ backgroundColor: '#000' }} />
                            <span>Adultos Mayores: {this.getCurrency(this.props.museum.prices.elderCurrency)} {this.getFormattedPrice(this.props.museum.prices.priceElder)}</span>
                        </div>
                        <div className="dividor-horizontal" style={{ backgroundColor: '#000' }} />
                        <h2>Horario</h2>
                        <div className="price-summary">
                            <div className="divider-vertical" style={{ backgroundColor: '#000' }} />
                            {this.props.museum.schedules.map(a => {
                                return (
                                    <React.Fragment>
                                        <span>{a.description}: {a.finishHour === 'Cerrado' ? 'Cerrado' : `${a.startHour} - ${a.finishHour}`}</span><div className="divider-vertical" style={{ backgroundColor: '#000' }} />
                                    </React.Fragment>
                                )
                            })}
                        </div>
                        {/* <div>
                            < Button fill type="button" onClick={() => this.setState({ currentFormPage: this.state.currentFormPage - 1 })}>
                                Volver
                                        </Button>
                            < Button fill type="button" bsStyle="primary" onClick={() => this.createUser()}>
                                Registrar
                                        </Button>
                        </div> */}
                    </React.Fragment>}
                </div>
            </div>
        )
    }
}
const mapStateToProps = ({ museumReducer, userReducer }) => ({
    waitingForResponce: museumReducer.waitingForResponce,
    museum: museumReducer.museum,
    error: museumReducer.error,
    currentUser: userReducer.currentUser
})

const mapDispatchToProps = dispatch => ({
    onFetchMuseumInfo: (val) => dispatch(fetchMuseuminfo(val))
})

export default
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(Profile)
