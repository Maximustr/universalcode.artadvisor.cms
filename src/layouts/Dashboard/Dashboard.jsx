import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import NotificationSystem from "react-notification-system";
import Header from "components/Header/Header";
import Sidebar from "components/Sidebar/Sidebar";


import { style } from "variables/Variables.jsx";

import dashboardRoutes from "routes/dashboard.jsx";

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.handleNotificationClick = this.handleNotificationClick.bind(this);
        this.state = {
            _notificationSystem: null
        };
    }
    handleNotificationClick(position, level, text, icon = 'pe-7s-gift') {
        this.setState({ _notificationSystem: this.refs.notificationSystem });
        this.refs.notificationSystem.addNotification({
            title: <span data-notify="icon" className={icon} />,
            message: (
                <div>
                    {text}
                </div>
            ),
            level: level,
            position: position,
            autoDismiss: 15
        });
    }

    render() {
        return (
            <div className="wrapper">
                <NotificationSystem ref="notificationSystem" style={style} />
                <Sidebar {...this.props} />
                <div id="main-panel" className="main-panel" ref="mainPanel">
                    <Header {...this.props} />
                    <Switch>
                        {dashboardRoutes.map((prop, key) => {
                            if (prop.needNotify)
                                return (
                                    <Route
                                        path={prop.path}
                                        key={key}
                                        render={routeProps => (
                                            <prop.component
                                                {...routeProps}
                                                handleClick={this.handleNotificationClick}
                                            />
                                        )}
                                    />
                                );
                            if (prop.redirect)
                                return <Redirect from={prop.path} to={prop.to} key={key} />;
                            return (
                                <Route path={prop.path} component={prop.component} key={key} />
                            );
                        })}
                    </Switch>
                </div>
            </div>
        );
    }
}

export default Dashboard;
