import React, { Component } from 'react'
import { connect } from 'react-redux'

import { unauthorizeRoutes, indexRoutes } from '../routes/index';
import { Route, Switch } from 'react-router-dom';
import NotificationSystem from "react-notification-system";
import { style } from "variables/Variables.jsx";
import Login from '../views/Login/Login';

class LayoutSwitch extends Component {
    state = {
        _notificationSystem: null
    }
    handleNotificationClick = (position, level, text, icon = 'pe-7s-gift') => {
        this.setState({ _notificationSystem: this.refs.notificationSystem });
        this.refs.notificationSystem.addNotification({
            title: <span data-notify="icon" className={icon} />,
            message: (
                <div>
                    {text}
                </div>
            ),
            level: level,
            position: position,
            autoDismiss: 3
        });
    }
    render() {
        const { uid, currentUser } = this.props
        const arrayOfRoutes = uid && currentUser ? indexRoutes : unauthorizeRoutes
        return (
            <div style={{ height: '100%' }}>
                <NotificationSystem ref="notificationSystem" style={style} />
                <Switch {...this.props}>
                    {arrayOfRoutes.map((route, key) => {
                        return <Route path={route.path} render={(props) => <route.component handleClick={this.handleNotificationClick} {...props} />} key={key} />;
                    })}
                    <Route component={Login} />
                </Switch>
            </div>
        )
    }
}

const mapStateToProps = ({ authReducer, userReducer }) => ({
    uid: authReducer.uid,
    currentUser: userReducer.currentUser
})
export default connect(mapStateToProps, null)(LayoutSwitch)
