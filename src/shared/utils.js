export const toObjArray = values => {
  const objArray = []
  values.forEach(a => {
    objArray.push({ ...a.data(), id: a.id })
  })
  return objArray;
}