import React from 'react';
import './Spinner.scss'

const Spinner = ({ isWhite }) => (
    <div className="spinner-wrapper">
        <div className={`lds-roller ${isWhite ? 'white' : null}`}>
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
        </div>
    </div>

);

export default Spinner;