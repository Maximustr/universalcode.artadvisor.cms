import React from 'react';
import './NotFound.scss'
const NotFound = () => {
    return (
        <div className="not-found-wrapper">
            <div className="color-wrapper">
                <div className="not-found-wrapper__conatiner">
                    <h1>404</h1>
                    <h2>Page not found!</h2>
                </div>
            </div>
        </div>
    );
};

export default NotFound;