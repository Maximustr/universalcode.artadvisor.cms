import React from 'react';
import ReactDOM from 'react-dom';
import * as ReactRedux from 'react-redux'
import { HashRouter, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/css/animate.min.css';
import './assets/sass/light-bootstrap-dashboard.css?v=1.2.0';
import './assets/css/demo.css';
import './assets/css/pe-icon-7-stroke.css';
import configureStore from './store/configureStore';
import { FIREBASE_CONFIG } from './constants';
import * as firebase from 'firebase'
import LayoutSwitch from './layouts/LayoutSwitch';
import './index.css'


firebase.initializeApp(FIREBASE_CONFIG)

const store = configureStore();
ReactDOM.render(
    <HashRouter>
        <ReactRedux.Provider store={store}>
            <Switch>
                <LayoutSwitch />
            </Switch>
        </ReactRedux.Provider>
    </HashRouter>,
    document.getElementById('root')
);
