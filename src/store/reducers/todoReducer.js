import { ADD_TODO, REMOVE_TODO, ADD_TODO_SUCCESS } from "../actions/todoActions";

const initialState = {
        todos: []
}

const todoReducer = (state = initialState, action) => {
        switch (action.type) {
                case ADD_TODO:
                        return {
                                ...state
                        }
                        case ADD_TODO_SUCCESS:
                        return {
                                ...state, todos: [...state.todos, action.payload]
                        }
                case REMOVE_TODO:
                        return {
                                ...state,
                                todos: state.todos.filter(todoToRemove => todoToRemove !== action.payload)
                        }
                default:
                        return {
                                ...state
                        }
        }
}
export default todoReducer

