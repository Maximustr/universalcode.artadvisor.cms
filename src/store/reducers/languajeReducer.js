import * as firebase from 'firebase'
import { toObjArray } from '../../shared/utils';

export const actionTypes = {
    FETCH_LANGUAGES: 'languajes/FETCH_LANGUAJES',
    FETCH_LANGUAGES_SUCCESS: 'languajes/FETCH_LANJUAGES_SUCCESS',
    FETCH_LANGAUGES_ERROR: 'languajes/FETCH_LANJAUGES_ERROR',

    FETCH_AVALIABLE: 'languajes/FETCH_AVALIABLE',
    FETCH_AVALIABLE_SUCCESS: 'languajes/FETCH_AVALIABLE_SUCCESS',
    FETCH_AVALIABLE_ERROR: 'languajes/FETCH_AVALIABLE_ERROR'
}

const initialState = {
    languages: [],
    waitingForResponce: false,
    error: null
}

export const languageReducer = (state = initialState, action) => {
    const { type, payload } = action
    switch (type) {
        case actionTypes.FETCH_AVALIABLE:
        case actionTypes.FETCH_LANGUAGES:
            return {
                ...state,
                waitingForResponce: true,
                error: false
            }
        case actionTypes.FETCH_AVALIABLE_SUCCESS:
        case actionTypes.FETCH_LANGUAGES_SUCCESS:
            return {
                ...state,
                waitingForResponce: false,
                languages: [...payload]
            }
        case actionTypes.FETCH_LANGAUGES_ERROR:
            return {
                ...state,
                waitingForREsponce: false,
                error: payload
            }
        default:
            return { ...state }
    }
}

export const fetchAllLanguages = () => dispatch => {
    dispatch({ type: actionTypes.FETCH_LANGUAGES })
    firebase.firestore().collection('language').get().then(res => {
        dispatch({ type: actionTypes.FETCH_LANGUAGES_SUCCESS, payload: toObjArray(res) })
    }).catch(err => {
        dispatch({ type: actionTypes.FETCH_LANGAUGES_ERROR })
    })
}
export const fetchAllAvaliableLanguajes = (artPieceId) => dispatch => {
    dispatch({ type: actionTypes.FETCH_AVALIABLE })
    firebase.firestore().collection('translations').doc(artPieceId).get().then(doc => {
        const artpieceID = doc.data().artPieceId;
        debugger
        firebase.firestore().collection('translations').where('artPieceId', '==', artpieceID).get().then(translations => {
            const array = toObjArray(translations);
            firebase.firestore().collection('language').get().then(res => {
                let allLanguajes = toObjArray(res)
                const avaliableLanguages = allLanguajes.filter(languageA => !array.find(languageB => languageB.language === languageA.id))
                dispatch({ type: actionTypes.FETCH_AVALIABLE_SUCCESS, payload: avaliableLanguages })
            }).catch(err => {
                dispatch({ type: actionTypes.FETCH_AVALIABLE_ERROR, payload: 'something went wrong' })
            })
        }).catch(err => {
            dispatch({ type: actionTypes.FETCH_AVALIABLE_ERROR, payload: 'something went wrong' })
        })
    }).catch(err => {
        dispatch({ type: actionTypes.FETCH_AVALIABLE_ERROR, payload: 'something went wrong' })
    })


}