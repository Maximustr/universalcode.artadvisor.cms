import * as firebase from 'firebase';


export const actionTypes = {
    FETCH_USER: 'user/FETCH_USER',
    FETCH_USER_SUCCESS: 'user/FETCH_USER_SUCCESS',
    FETCH_USER_ERROR: 'user/FETCH_USER_ERROR',

    CREATE_USER: 'user/CREATE_USER',
    CREATE_USER_SUCCESS: 'user/CREATE_USER_SUCCESS',
    CREATE_USER_ERROR: 'user/CREATE_USER_ERROR',


}

const initialstate = {
    currentUser: null,
    waitingforResponce: false,
    error: null,
    userMuseum: null
}

export const userReducer = (state = initialstate, action) => {
    const { type, payload } = action
    switch (type) {

        case actionTypes.FETCH_USER:
        case actionTypes.CREATE_USER:
            return {
                ...state,
                waitingforResponce: true,
                error: null
            }
        case actionTypes.FETCH_USER_SUCCESS:
            return {
                ...state,
                waitingforResponce: false,
                currentUser: payload.user,
                userMuseum: payload.museum
            }
        case actionTypes.CREATE_USER_SUCCESS:
            return {
                ...state,
                waitingforResponce: false,
                error: false
            }
        case actionTypes.CREATE_USER_ERROR:
        case actionTypes.FETCH_USER_ERROR:
            return {
                ...state,
                waitingforResponce: false,
                error: payload
            }
        default:
            return { ...state }
    }
}

export const fetchUser = (uid) => dispatch => {

    dispatch({ type: actionTypes.FETCH_USER })
    firebase.firestore().collection('users').where('uid', '==', uid).get().then(querySnapshot => {
        let user
        querySnapshot.forEach((doc) => {
            user = { ...doc.data(), id: doc.id }
        });
        firebase.firestore().collection('museum').where('userId', '==', user.id).get().then(res => {
            let museum
            res.forEach((doc) => {
                museum = { ...doc.data(), id: doc.id }
            });
            dispatch({ type: actionTypes.FETCH_USER_SUCCESS, payload: { user, museum } })
        }).catch(err => {
            dispatch({ type: actionTypes.FETCH_USER_ERROR, payload: 'usuario no encontrado' })
        })
    }).catch(err => {
        dispatch({ type: actionTypes.FETCH_USER_ERROR, payload: 'usuario no encontrado' })

    })
}
const getCurrencyTypeBecauseOfAndroidBadDesing = (correctCurrencyCode) => {
    switch (correctCurrencyCode) {
        case 'crc':
            return '₡'
        case 'usd':
            return '$'
        case 'eur':
            return '€'
        default:
            return
    }
}
const getFormattedPrice = (val) => {
    const formatterCr = new Intl.NumberFormat('es-CR', {
        style: 'currency',
        currency: 'CRC',
        minimumFractionDigits: 2
    })
    return formatterCr.format(val).slice(1);
}

export const createUser = (values, exectFunc) => dispatch => {

    const { email, password } = values.userDetails

    dispatch({ type: actionTypes.CREATE_USER })
    firebase.auth().createUserWithEmailAndPassword(email, password).then((res => {
        firebase.firestore().collection('users').add({ email, type: 'MUSEUM_ADMIN', uid: res.user.uid }).then((userDoc) => {
            const museumToAdd = {
                address: values.description,
                description: values.museumDetails.museumDesc,
                email,
                latitud: values.location.lat,
                link: values.museumDetails.museumLink,
                longitud: values.location.lng,
                points: "2000",
                stars: "0",
                name: values.museumDetails.museumName,
                userId: userDoc.id,
                phoneNumber: values.museumDetails.museumPhoneNumber
            }
            firebase.firestore().collection('museum').add(museumToAdd).then(museumCreatedDoc => {
                firebase.storage().ref(`/Fotos/imagenesdelmuseo/${values.museumDetails.museumName}-image`).put(values.museumDetails.museumImage).then(imageSaved => {
                    firebase.storage().ref('/Fotos/imagenesdelmuseo').child(`${values.museumDetails.museumName}-image`).getDownloadURL().then(downloadUrl => {
                        museumToAdd.idMuseum = museumCreatedDoc.id
                        museumToAdd.img = downloadUrl
                        firebase.firestore().collection('museum').doc(museumCreatedDoc.id).set(museumToAdd).then(final => {
                            const elderPrice = {
                                description: 'Adulto mayor',
                                idMuseum: museumCreatedDoc.id,
                                type: getCurrencyTypeBecauseOfAndroidBadDesing(values.prices.elderCurrency),
                                price: `${getFormattedPrice(values.prices.priceElder)}`
                            }
                            const adultPrice = {
                                description: 'Adulto',
                                idMuseum: museumCreatedDoc.id,
                                type: getCurrencyTypeBecauseOfAndroidBadDesing(values.prices.adultCurrency),
                                price: `${getFormattedPrice(values.prices.priceAdult)}`
                            }
                            const studentPrice = {
                                description: 'Estudiantes',
                                idMuseum: museumCreatedDoc.id,
                                type: getCurrencyTypeBecauseOfAndroidBadDesing(values.prices.studentCurrency),
                                price: `${getFormattedPrice(values.prices.priceStudent)}`
                            }
                            const childrenPrice = {
                                description: 'Niños',
                                idMuseum: museumCreatedDoc.id,
                                type: getCurrencyTypeBecauseOfAndroidBadDesing(values.prices.childCurrency),
                                price: `${getFormattedPrice(values.prices.priceChildren)}`
                            }
                            const prices = [childrenPrice, studentPrice, adultPrice, elderPrice]
                            const promisesArray = prices.map(price => firebase.firestore().collection('price').add(price))
                            Promise.all(promisesArray).then(documents => {
                                //making more useless promises because of bad android desing
                                const uselessPromises = documents.map(document => {
                                    return firebase.firestore().collection('price').doc(document.id).get()
                                })
                                Promise.all(uselessPromises).then(result => {
                                    const getpricesToUpdatePromices = result.map(fbDoc => {

                                        return firebase.firestore().collection('price').doc(fbDoc.id).set({ ...fbDoc.data(), idPrice: fbDoc.id })
                                    }
                                    )
                                    Promise.all(getpricesToUpdatePromices).then(allPricesUpdatedResponce => {
                                        const schedules = [
                                            {
                                                description: 'L',
                                                finishHour: values.time.mondayHO === "00:00" && values.time.mondayH === "00:00" ? 'Cerrado' : `${values.time.mondayHO} pm`,
                                                startHour: values.time.mondayHO === "00:00" && values.time.mondayH === "00:00"  ? 'Cerrado' : `${values.time.mondayH} ${parseInt(values.time.mondayH.substr(0, 2)) > 12 ? 'pm' : 'am'}`,
                                                idMuseum: museumCreatedDoc.id
                                            },
                                            {
                                                description: 'M',
                                                finishHour: values.time.tuesdayHO === "00:00" && values.time.tuesdayH === "00:00" ? 'Cerrado' : `${values.time.tuesdayHO} pm`,
                                                startHour: values.time.tuesdayHO === "00:00" && values.time.tuesdayH === "00:00" ? 'Cerrado' : `${values.time.tuesdayH} ${parseInt(values.time.tuesdayH.substr(0, 2)) > 12 ? 'pm' : 'am'}`,
                                                idMuseum: museumCreatedDoc.id
                                            },
                                            {
                                                description: 'K',
                                                finishHour: values.time.wednesayHO === "00:00"&& values.time.wednesayH === "00:00"? 'Cerrado' : `${values.time.wednesayHO} pm`,
                                                startHour: values.time.wednesayHO === "00:00"&& values.time.wednesayH === "00:00" ? 'Cerrado' : `${values.time.wednesayH} ${parseInt(values.time.wednesayH.substr(0, 2)) > 12 ? 'pm' : 'am'}`,
                                                idMuseum: museumCreatedDoc.id
                                            },
                                            {
                                                description: 'J',
                                                finishHour: values.time.thursdayHO === "00:00" && values.time.thursdayH === "00:00" ? 'Cerrado' : `${values.time.thursdayHO} pm`,
                                                startHour: values.time.thursdayHO === "00:00" && values.time.thursdayH === "00:00" ? 'Cerrado' : `${values.time.thursdayH} ${parseInt(values.time.thursdayH.substr(0, 2)) > 12 ? 'pm' : 'am'}`,
                                                idMuseum: museumCreatedDoc.id
                                            },
                                            {
                                                description: 'V',
                                                finishHour:values.time.fridayH === "00:00"&&values.time.fridayHO === "00:00"? 'Cerrado' : `${values.time.fridayHO} pm`,
                                                startHour: values.time.fridayH === "00:00"&&values.time.fridayHO === "00:00"? 'Cerrado' : `${values.time.fridayH} ${parseInt(values.time.fridayH.substr(0, 2)) > 12 ? 'pm' : 'am'}`,
                                                idMuseum: museumCreatedDoc.id
                                            },
                                            {
                                                description: 'S',
                                                finishHour:values.time.saturdayH === "00:00"&&values.time.saturdayHO === "00:00"? 'Cerrado' : `${values.time.saturdayHO} pm`,
                                                startHour: values.time.saturdayH === "00:00"&&values.time.saturdayHO === "00:00"? 'Cerrado' : `${values.time.saturdayH} ${parseInt(values.time.saturdayH.substr(0, 2)) > 12 ? 'pm' : 'am'}`,
                                                idMuseum: museumCreatedDoc.id
                                            },
                                            {
                                                description: 'D',
                                                finishHour:values.time.sundayH === "00:00"&&values.time.sundayHO === "00:00"? 'Cerrado' : `${values.time.sundayHO} pm`,
                                                startHour: values.time.sundayH === "00:00"&&values.time.sundayHO === "00:00"? 'Cerrado' : `${values.time.sundayH} ${parseInt(values.time.sundayH.substr(0, 2)) > 12 ? 'pm' : 'am'}`,
                                                idMuseum: museumCreatedDoc.id
                                            }
                                        ].map(schedule => firebase.firestore().collection('schedule').add(schedule))
                                        Promise.all(schedules).then(shedules => {
                                            const evenMoreUselessPromises = shedules.map(FbDocument => {

                                                return firebase.firestore().collection('schedule').doc(FbDocument.id).get()
                                            })
                                            Promise.all(evenMoreUselessPromises).then(toUpdatedSchedules => {
                                                const updatedSchedules = toUpdatedSchedules.map(doc => {
                                                    return firebase.firestore().collection('schedule').doc(doc.id).set({ ...doc.data(), idSchedule: doc.id })
                                                })
                                                Promise.all(updatedSchedules).then(uselessResponce => {
                                                    dispatch({ type: actionTypes.CREATE_USER_SUCCESS })
                                                    exectFunc()
                                                }).catch(err => {
                                                    dispatch({ type: actionTypes.CREATE_USER_ERROR })
                                                    exectFunc("Error")
                                                })
                                            }).catch(err => {
                                                dispatch({ type: actionTypes.CREATE_USER_ERROR })
                                                exectFunc("Error")
                                            })
                                        }).catch(err => {
                                            dispatch({ type: actionTypes.CREATE_USER_ERROR })
                                            exectFunc("Error")
                                        })
                                    }).catch(err => {
                                        dispatch({ type: actionTypes.CREATE_USER_ERROR })
                                        exectFunc("Error")
                                    })
                                }).catch(err => {
                                    dispatch({ type: actionTypes.CREATE_USER_ERROR })
                                    exectFunc("Error")
                                })
                            }).catch(err => {
                                dispatch({ type: actionTypes.CREATE_USER_ERROR })
                                exectFunc("Error")
                            })

                        }).catch(err => {
                            dispatch({ type: actionTypes.CREATE_USER_ERROR })
                            exectFunc("Error")
                        })
                    }).catch(err => {
                        dispatch({ type: actionTypes.CREATE_USER_ERROR })
                        exectFunc("Error")
                    })
                }).catch(err => {
                    dispatch({ type: actionTypes.CREATE_USER_ERROR })
                    exectFunc("Error")
                })
                //spanish variable names because of bad android app desing
                //set id because of bad desing of android app 
                //set image to firebase
            }).catch(err => {
                dispatch({ type: actionTypes.CREATE_USER_ERROR })
                exectFunc("Error")
            })

        }).catch(err => {
            dispatch({ type: actionTypes.CREATE_USER_ERROR })
            exectFunc("Error")
        })
    })).catch(function (error) {
        dispatch({ type: actionTypes.CREATE_USER_ERROR })
        exectFunc("Error")
    });
}



