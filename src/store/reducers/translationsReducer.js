import * as firebase from 'firebase'
import { toObjArray } from '../../shared/utils';
const actionTypes = {
    RESET_VALUES: 'translations/RESET_VALUES',
    FETCH_ALL_TRANSLATIONS: 'translations/FETCH_ALL_TRANSLATIONS',
    FETCH_ALL_TRANSLATIONS_SUCCESS: 'translations/FETCH_ALL_TRANSLATIONS_SUCCESS',
    FETCH_ALL_TRANSLATIONS_ERROR: 'translations/FETCH_ALL_TRANSLATIONS_ERROR',

    CREATE_TRANSLATION: 'translations/CREATE_TRANSLATION',
    CREATE_TRANSLATION_SUCCESS: 'translations/CREATE_TRANSLATION_SUCCESS',
    CREATE_TRANSLATION_ERROR: 'translations/CREATE_TRANSLATION_ERROR',

    UPDATE_TRANSLATION: 'translations/UPDATE_TRANSLATION',
    UPDATE_TRANSLATION_SUCCESS: 'translations/UPDATE_TRANSLATION_SUCCESS',
    UPDATE_TRANSLATION_ERROR: 'translations/UPDATE_TRANSLATION_ERROR',

    REMOVE_TRANSLATION: 'translations/REMOVE_TRANSLATION',
    REMOVE_TRANSLATION_SUCCESS: 'translations/REMOVE_TRANSLATION_SUCCESS',
    REMOVE_TRANSLATIONS_ERROR: 'translations/REMOVE_TRANSLATION'
}

const initialState = {

    translations: [],
    waitingForResponce: false,
    error: null,
    added: false
}

export const translationReducer = (state = initialState, action) => {
    const { type, payload } = action
    switch (type) {
        case actionTypes.RESET_VALUES:
            return {
                ...state, waitingForResponce: false,
                error: null,
                added: false
            }
        case actionTypes.UPDATE_TRANSLATION:
        case actionTypes.CREATE_TRANSLATION:
        case actionTypes.REMOVE_TRANSLATION:
        case actionTypes.FETCH_ALL_TRANSLATIONS:
            return {
                ...state,
                waitingForResponce: true,
                error: null
            }
        case actionTypes.FETCH_ALL_TRANSLATIONS_SUCCESS:
            return {
                ...state,
                waitingForResponce: false,
                translations: payload,
            }
        case actionTypes.UPDATE_TRANSLATION_SUCCESS:
            return {
                ...state,
                waitingForResponce: false,
            }
        case actionTypes.CREATE_TRANSLATION_SUCCESS:
            return {
                ...state,
                waitingForResponce: false,
                traslation: [...state.translations, payload]
            }
        case actionTypes.REMOVE_TRANSLATION_SUCCESS:
            return {
                ...state,
                waitingForResponce: false,
                translations: state.translations.filter(a => a.id !== payload),
            }
        case actionTypes.UPDATE_TRANSLATION_ERROR:
        case actionTypes.CREATE_TRANSLATION_ERROR:
        case actionTypes.REMOVE_TRANSLATIONS_ERROR:
        case actionTypes.FETCH_ALL_TRANSLATIONS_ERROR:
            return {
                ...state,
                waitingForResponce: false,
                error: payload
            }
        default:
            return {
                ...state
            }
    }
}


export const fetchAllTranstaltions = ({ museumId, pieceId }) => dispatch => {
    dispatch({ type: actionTypes.FETCH_ALL_TRANSLATIONS })
    let colletion = firebase.firestore().collection('translations').where('museumId', '==', museumId)
    if (pieceId) {
        colletion = colletion.where('artPieceId', '==', pieceId)
    }
    colletion.get().then(res => {
        dispatch({ type: actionTypes.FETCH_ALL_TRANSLATIONS_SUCCESS, payload: toObjArray(res) })
    }).catch(err => {
        dispatch({ type: actionTypes.FETCH_ALL_TRANSLATIONS_ERROR, payload: 'something went wrong' })
    })
}

export const createTranslation = (values, notifyFunc) => dispatch => {
    const collection = firebase.firestore().collection('translations')
    const translationToSend = {
        Translation: values.Translation,
        artPieceId: values.artPieceId,
        language: values.language,
        museumId: values.museumId
    }
    firebase.firestore().collection('language').get().then(res => {
        const allLanguages = toObjArray(res);
        firebase.firestore().collection('translations').where('artPieceId', '==', translationToSend.artPieceId).get().then(artPieceTranslations => {
            const allTransations = toObjArray(artPieceTranslations)
            const avaliableLanguages = allLanguages.filter(languageA => !allTransations.find(languageB => languageB.language === languageA.id))
            if (values.id) {
                firebase.firestore().collection('translations').doc(values.id).get().then(translationResponce => {
                    const translationObj = { ...translationResponce.data(), id: translationResponce.id }
                    if (translationObj.museumId === translationToSend.museumId) {
                        if (avaliableLanguages.find(a => a.id === translationToSend.language) || translationToSend.language === translationObj.language) {
                            dispatch({ type: actionTypes.UPDATE_TRANSLATION })
                            collection.doc(values.id).set(translationToSend).then(res => {
                                dispatch({ type: actionTypes.UPDATE_TRANSLATION_SUCCESS })
                                notifyFunc()
                            }).catch(err => {
                                dispatch({ type: actionTypes.UPDATE_TRANSLATION_ERROR })
                                notifyFunc("Algo salio mal")
                            })
                        } else {
                            notifyFunc('La obra seleccionada ya cuenta con el lenguaje')
                        }
                    } else {
                        if (avaliableLanguages.find(a => a.id === translationToSend.language)) {
                            collection.doc(values.id).set(translationToSend).then(res => {
                                dispatch({ type: actionTypes.UPDATE_TRANSLATION_SUCCESS })
                                notifyFunc()
                            }).catch(err => {
                                dispatch({ type: actionTypes.UPDATE_TRANSLATION_ERROR })
                                notifyFunc("Algo salio mal")
                            })
                        } else {
                            notifyFunc('La obra seleccionada ya cuenta con el lenguaje')
                        }
                    }
                })
            } else {
                if (avaliableLanguages.find(a => a.id === translationToSend.language)) {
                    dispatch({ type: actionTypes.CREATE_TRANSLATION })
                    collection.add(translationToSend).then(res => {
                        dispatch({ type: actionTypes.CREATE_TRANSLATION_SUCCESS, payload: { ...values, id: res.id } })
                        notifyFunc()
                    }).catch(err => {
                        dispatch({ type: actionTypes.CREATE_TRANSLATION_ERROR })
                        notifyFunc("Algo salio mal")
                    })
                } else {
                    dispatch({ type: actionTypes.CREATE_TRANSLATION_ERROR })
                    notifyFunc('La obra seleccionada ya cuenta con el lenguaje')
                }
            }

        }).catch(err => {
            dispatch({ type: actionTypes.CREATE_TRANSLATION_ERROR })
            notifyFunc('La obra seleccionada ya cuenta con el lenguaje')
        })
    }).catch(err => {
        dispatch({ type: actionTypes.CREATE_TRANSLATION_ERROR })
        notifyFunc("Algo salio mal")
    })
}
export const removeTranslation = (id, func) => dispatch => {
    dispatch({ type: actionTypes.REMOVE_TRANSLATION })
    firebase.firestore().collection('translations').doc(id).delete().then(res => {
        dispatch({ type: actionTypes.REMOVE_TRANSLATION_SUCCESS, payload: id })
        func()
    }).catch(err => {
        func(true)
        dispatch({ type: actionTypes.REMOVE_TRANSLATIONS_ERROR })
    })
}
export const resetValues = () => dispatch => {
    dispatch({ type: actionTypes.RESET_VALUES })
}