import * as firebase from 'firebase';

export const actionTypes = {
    LOGIN: 'auth/LOGIN',
    LOGIN_SUCCESS: 'auth/LOGIN_SUCCESS',
    LOGIN_ERROR: 'auth/LOGIN_ERROR',
    LOGOUT: 'auth/LOGOUT',

    FORGOT_PASSWORD: 'auth/FORGOT_PASSWORD',
    FORGOT_PASSWORD_SUCCESS: 'auth/FORGOT_PASSWORD_SUCCESS',
    FORGOT_PASSWORD_ERROR: 'auth/FORGOT_PASSWORD_ERROR',
}
const initialState = {
    uid: null,
    waitingForResponce: false,
    error: null
}
export const authReducer = (state = initialState, action) => {
    const { type, payload } = action
    switch(type){
        case actionTypes.FORGOT_PASSWORD:
        case actionTypes.LOGIN:
            return {
                ...state,
                error: null,
                waitingForResponce: true
            }
        case actionTypes.LOGIN_SUCCESS:
            return{
                ...state,
                waitingForResponce: false,
                uid: payload.uid,
            }
        case actionTypes.FORGOT_PASSWORD_SUCCESS:
            return {
                ...state,
                waitingForResponce: false,
            }
        case actionTypes.FORGOT_PASSWORD_ERROR:
        case actionTypes.LOGIN_ERROR:
            return{
                ...state,
                waitingForResponce:false,
                error:payload
            }
        case actionTypes.LOGOUT:
            return {
                uid: null,
                waitingForResponce: false,
                error: null
            }
        default:
            return{...state}
    }
}

export const loginUser = (credentials,successFunc, errFunc) => dispatch => {
    dispatch({type:actionTypes.LOGIN})
    firebase.auth().signInWithEmailAndPassword(credentials.email, credentials.password).then(res=>{
        firebase.firestore().collection('users').where('uid', '==', res.user.uid).where('type','==','MUSEUM_ADMIN').get().then(doc=>{
            if(!doc.empty){    
                successFunc(res.user.uid)
                dispatch({type:actionTypes.LOGIN_SUCCESS,payload:res.user})
            }else{
                dispatch({type:actionTypes.LOGIN_ERROR})
                errFunc("Usuario no permitido")
            }            
        }).catch(err=>{
            dispatch({type:actionTypes.LOGIN_ERROR})
            errFunc("Usuario no permitido")
        })    
    }).catch((error)=> {
        dispatch({type:actionTypes.LOGIN_ERROR})
        errFunc("Correo y contraseña no coinciden")
    });
}
export const forgotPassword = (email ,  onSuccFunc, onErrFunc) => dispatch => {
    dispatch({type:actionTypes.FORGOT_PASSWORD})
    firebase.auth().sendPasswordResetEmail(email).then(res=>{
        dispatch({type:actionTypes.FORGOT_PASSWORD_SUCCESS})
        onSuccFunc()
    }).catch(err=>{
        dispatch({type:actionTypes.FORGOT_PASSWORD_ERROR})
        onErrFunc("Correo no registrado")
    })
}

export const logoutUser = () =>dispatch=>{
    dispatch({type:actionTypes.LOGOUT})
}
