import * as firebase from 'firebase'
import { toObjArray } from '../../shared/utils';
const actionTypes = {
    FETCH_ART_PIECES: 'art-pieces/FETCH_ART_PIECES',
    FETCH_ART_PIECES_SUCCESS: 'art-pieces/FETCH_ART_PIECES_SUCCESS',
    FETCH_ART_PIECES_ERROR: 'art-pieces/FETCH_ART_PIECES_ERROR'
}
const initialState = {
    waitingForResponce: false,
    artPieces: [],
    error: null
}
export const artPiecesReducer = (state = initialState, action) => {
    const { type, payload } = action
    switch (type) {
        case actionTypes.FETCH_ART_PIECES:
            return {
                ...state,
                waitingForResponce: true,
                error: null
            }
        case actionTypes.FETCH_ART_PIECES_SUCCESS:
            return {
                ...state,
                waitingForResponce: false,
                artPieces: payload
            }
        case actionTypes.FETCH_ART_PIECES_ERROR:
            return {
                ...state,
                waitingForResponce: false,
                error: payload
            }
        default:
            return {
                ...state
            }
    }
}



export const fetchArtPieces = ({ museumId }) => dispatch => {
    dispatch({ type: actionTypes.FETCH_ART_PIECES })
    firebase.firestore().collection('art-piece').where('museumId', '==', museumId).get().then(res => {
        dispatch({ type: actionTypes.FETCH_ART_PIECES_SUCCESS, payload: toObjArray(res) })
    }).catch(err => {
        dispatch({ type: actionTypes.FETCH_ART_PIECES_ERROR, payload: 'Something went wrong' })
    })
}
