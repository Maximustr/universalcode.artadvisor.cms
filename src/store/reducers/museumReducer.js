import * as firebase from 'firebase';

export const actionTypes = {
    FETCH_MUSEUM_INFO: 'museum/FETCH_MUSEUM_INFO',
    FETCH_MUSEUM_INFO_SUCCESS: 'museum/FETCH_MUSEUM_INFO_SUCCESS',
    FETCH_MUSEUM_INFO_ERROR: 'muesum/FETCH_MUSEUM_INFO_ERROR'
}
const initialState = {
    museum: null,
    waitingForResponce: true,
    error: null
}

export const museumReducer = (state = initialState, action) => {
    const { type, payload } = action
    switch (type) {
        case actionTypes.FETCH_MUSEUM_INFO:
            return {
                ...state,
                waitingForResponce: true,
                error: null
            }
        case actionTypes.FETCH_MUSEUM_INFO_SUCCESS:
            return {
                ...state,
                museum: payload,
                waitingForResponce: false
            }
        case actionTypes.FETCH_MUSEUM_INFO_ERROR:
            return {
                ...state,
                waitingForResponce: false,
                error: payload
            }
        default:
            return {
                ...state
            }
    }
}
// const parseDates = (date) => {
//     switch () {

//     }
// }

const parseCurrency = (sing) => {
    switch (sing) {
        case '₡':
            return 'crc'
        case '$':
            return 'usd'
        case '€':
            return 'eur'
    }
}



export const getMoneyInt = (a) => {
    return parseInt(a.replace(',', '').replace(/\s/g, ''))
}
export const fetchMuseuminfo = userId => dispatch => {
    dispatch({ type: actionTypes.FETCH_MUSEUM_INFO })
    firebase.firestore().collection('museum').where('userId', '==', userId).get().then(museumDoc => {
        if (museumDoc.empty) {
            dispatch({ type: actionTypes.FETCH_MUSEUM_INFO_ERROR })
        } else {
            const museum = museumDoc.docs[0].data()
            firebase.firestore().collection('price').where('idMuseum', '==', museum.idMuseum).get().then(pricesDocs => {
                if (pricesDocs.empty) {
                    dispatch({ type: actionTypes.FETCH_MUSEUM_INFO_ERROR })
                } else {
                    const prices = {}
                    const pricesFromDb = pricesDocs.docs.map(a => a.data())
                    pricesFromDb.forEach(a => {
                        if (a.description === 'Adulto mayor') {
                            prices.priceElder = getMoneyInt(a.price)
                            prices.elderCurrency = parseCurrency(a.type)
                        }
                        if (a.description === 'Niños') {
                            prices.priceChildren = getMoneyInt(a.price)
                            prices.childCurrency = parseCurrency(a.type)
                        }
                        if (a.description === 'Adulto') {
                            prices.priceAdult = getMoneyInt(a.price)
                            prices.adultCurrency = parseCurrency(a.type)
                        }
                        if (a.description === 'Estudiantes') {
                            prices.priceStudent = getMoneyInt(a.price)
                            prices.studentCurrency = parseCurrency(a.type)
                        }
                    })
                    firebase.firestore().collection('schedule').where('idMuseum', '==', museum.idMuseum).get().then(schedulesDocs => {
                        if (schedulesDocs.empty) {
                            dispatch({ type: actionTypes.FETCH_MUSEUM_INFO_ERROR })
                        } else {
                            const schedulesFromDb = schedulesDocs.docs.map(doc => doc.data())
                            const schedulesToShow = []
                            schedulesToShow.push(schedulesFromDb.find(a => a.description === 'L'))
                            schedulesToShow.push(schedulesFromDb.find(a => a.description === 'M'))
                            schedulesToShow.push(schedulesFromDb.find(a => a.description === 'K'))
                            schedulesToShow.push(schedulesFromDb.find(a => a.description === 'J'))
                            schedulesToShow.push(schedulesFromDb.find(a => a.description === 'V'))
                            schedulesToShow.push(schedulesFromDb.find(a => a.description === 'S'))
                            schedulesToShow.push(schedulesFromDb.find(a => a.description === 'D'))
                            museum.schedules = schedulesToShow
                            museum.prices = prices
                            dispatch({ type: actionTypes.FETCH_MUSEUM_INFO_SUCCESS, payload: museum })
                        }
                    }).catch(err => {
                        dispatch({ type: actionTypes.FETCH_MUSEUM_INFO_ERROR })
                    })
                }
            }).catch(err => {
                dispatch({ type: actionTypes.FETCH_MUSEUM_INFO_ERROR })
            })
        }
    }).catch(err => {
        dispatch({ type: actionTypes.FETCH_MUSEUM_INFO_ERROR })
    })
}