import { createStore, combineReducers, compose, applyMiddleware } from 'redux'
import logger from 'redux-logger';
import thunk from 'redux-thunk'

import { artPiecesReducer } from './reducers/artPiecesReducer';
import { languageReducer } from './reducers/languajeReducer';
import { translationReducer } from './reducers/translationsReducer';
import { userReducer } from './reducers/userReducer';
import { authReducer } from './reducers/authReducer';
import { museumReducer } from './reducers/museumReducer';


const middlewares = applyMiddleware(thunk, logger)
const rootReducer = combineReducers({
  artPiecesReducer,
  languageReducer,
  translationReducer,
  userReducer,
  authReducer,
  museumReducer
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const configureStore = () =>
  createStore(rootReducer, composeEnhancers(middlewares))

export default configureStore
